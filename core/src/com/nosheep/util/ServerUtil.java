package com.nosheep.util;

import com.nosheep.game.combatuser.Enemy;
import com.nosheep.handler.server.ServerHandler;
import com.nosheep.model.skill.Skill;

public class ServerUtil {

    public static ServerHandler SERVER_HANDLER;

    public static void sendUsedSkill(Skill skill) {
        SERVER_HANDLER.sendUsedSkill(skill);
    }

    public static void sendEnemyDied(Enemy enemy) {
        SERVER_HANDLER.sendEnemyDied(enemy);
    }
}
