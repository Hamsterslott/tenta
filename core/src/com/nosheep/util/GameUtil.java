package com.nosheep.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.*;
import com.badlogic.gdx.math.*;
import com.nosheep.game.asset.AssetLoader;
import com.nosheep.game.combatuser.Enemy;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.game.combatuser.Player;
import com.nosheep.game.TentaGame;
import com.nosheep.interfaces.Item;
import com.nosheep.screen.DefaultScreen;
import com.nosheep.screen.map.MapScreen;
import com.nosheep.screen.ui.component.Window;

/**
 * Created by Johan on 2017-07-21.
 */
public class GameUtil {
    public static final String TITLE = "Tenta";
    public static final String VERSION = " v.1.0";
    public static TentaGame GAME;
    public static String USERNAME = "";
    public static MapScreen currentMap;

    public static void setMapScreen(MapScreen screen) {
        setCurrentMap(screen);
        GAME.setScreen(screen);
        if (GAME.player != null) {
            getPlayer().setPosition(currentMap.startingPoint.x, currentMap.startingPoint.y);
            getPlayer().setLocation(currentMap.getClass().getSimpleName());
        }
    }
    public static void setMapScreen(MapScreen screen, Vector2 targetMapPosition) {
        setCurrentMap(screen);
        GAME.setScreen(screen);
        if (GAME.player != null) {
            getPlayer().setPosition(targetMapPosition.x, targetMapPosition.y);
            getPlayer().setLocation(currentMap.getClass().getSimpleName());
            getPlayer().setTarget(null);
        }
    }

    public static void setCurrentMap(MapScreen mapScreen) {
        currentMap = mapScreen;
    }

    public static boolean isOverlapingMapObject(Rectangle rectangle) {
        // As of now only PolygonMapObjects gives collision, strange..
        try{
            for (MapObject object : currentMap.mapObjects) {
                if (object instanceof PolygonMapObject) {
                    Rectangle r = ((PolygonMapObject) object).getPolygon().getBoundingRectangle();
                    if (r.overlaps(rectangle)) {
                        return true;
                    }
                }
            }
        }
        catch(NullPointerException ex) {}
        return false;
    }

    public static boolean isOverlaping(Rectangle r1, Rectangle r2) {
        if(r1.overlaps(r2)) return true;
        return false;
    }

    public static Item getItemByName(String name) {
        Item item = null;
        for(Item i : AssetLoader.itemList) {
            if(i.getClass().getSimpleName().equals(name)) {
                item = i;
                break;
            }
        }
        try {
            return (Item) Class.forName(item.getClass().getName()).newInstance();
        } catch (Exception e) {
            Gdx.app.log("Error: getItemByName: ", e.getMessage());
            return item;
        }
    }

    public static Enemy getEnemyById(int id) {
        for (Enemy e : currentMap.enemies) {
            if (e.getId().equals(Integer.toString(id)))
                return e;
        }
        return null;
    }

    public static void changeEmitterAngle(ParticleEffect particleEffect, String emitterName, float targetAngle) {
        ParticleEmitter emitter = particleEffect.findEmitter(emitterName);
        ParticleEmitter.ScaledNumericValue angle = emitter.getAngle();
        float angleHighMin = angle.getHighMin();
        float angleHighMax = angle.getHighMax();
        float spanHigh = angleHighMax - angleHighMin;
        angle.setHigh(targetAngle - spanHigh / 2.0f, targetAngle + spanHigh / 2.0f);
        float angleLowMin = angle.getLowMin();
        float angleLowMax = angle.getLowMax();
        float spanLow = angleLowMax - angleLowMin;
        angle.setLow(targetAngle - spanLow / 2.0f, targetAngle + spanLow / 2.0f);
    }

    public static void setCombatUserTarget(CombatUser target, CombatUser combatUser) {
        combatUser.setTarget(target);
    }

    public static Player getPlayer() {
        return GAME.player;
    }

}
