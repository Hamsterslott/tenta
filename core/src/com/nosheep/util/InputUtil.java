package com.nosheep.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.screen.DefaultScreen;
import com.nosheep.screen.ui.component.Window;

public class InputUtil {
    public static boolean isMouseLeftClickedOutsideWindow() {
        if(!isClickEventOnAWindow()
                && Gdx.input.justTouched()
                && Gdx.input.isButtonPressed(Input.Buttons.LEFT))
            return true;

        return false;
    }

    public static boolean isMouseRightClickedOutsideWindow() {
        if(!isClickEventOnAWindow()
                && Gdx.input.justTouched()
                && Gdx.input.isButtonPressed(Input.Buttons.RIGHT))
            return true;

        return false;
    }

    private static boolean isClickEventOnAWindow() {
        for(Window w : GameUtil.currentMap.ui.windows) {
            if(w.isHidden()) continue;
            else if(isMouseHovering(w.getBounds()) && Gdx.input.justTouched()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMouseHovering(Rectangle rectangle) {
        Vector2 mouse = getMouseCoordinates();
        if (rectangle.contains(mouse))
            return true;
        return false;
    }

    public static Vector2 getMouseCoordinates() {
        Vector2 mouse = new Vector2();
        mouse.x = Gdx.input.getX();
        mouse.y = Gdx.input.getY();
        ((DefaultScreen) GameUtil.GAME.getScreen()).getViewport().unproject(mouse);
        return mouse;
    }
}
