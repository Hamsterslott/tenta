package com.nosheep.util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.nosheep.game.asset.AssetLoader;
import com.nosheep.game.asset.animation.AnimatedSprite;

public class AssetUtil {

    public static AssetLoader ASSET_LOADER;

    public static Texture getTexture(String name) {
        return ASSET_LOADER.getTexture(name);
    }

    public static Sound getSound(String name) {
        return ASSET_LOADER.getSound(name);
    }

    public static BitmapFont getFont(String name) {
        return ASSET_LOADER.getFont(name);
    }

    public static Skin getSkin(String name) {
        return ASSET_LOADER.getSkin(name);
    }

    public static AnimatedSprite getAnimatedSprite(String name, boolean loop) {
        return ASSET_LOADER.getAnimatedSprite(name, loop);
    }

    public static ParticleEffect getParticleEffect(String pfPath) {
        return ASSET_LOADER.getParticleEffect(pfPath);
    }

}
