package com.nosheep.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.nosheep.game.asset.AssetLoader;
import com.nosheep.game.asset.animation.AnimatedSprite;

/**
 * Created by Johan on 2017-07-23.
 */
public class GraphicUtil {

    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;

    public static final int WINDOW_WIDTH = 1280;
    public static final int WINDOW_HEIGHT = WINDOW_WIDTH / 16 * 9;
    public static final int BACKGROUND_FPS = 60;
    public static final int FOREGROUND_FPS = 60;

    public static void setFullScreen(){
        Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
    }

    public static void setWindowed(){
        Gdx.graphics.setWindowedMode(WINDOW_WIDTH, WINDOW_HEIGHT);
    }

    public static void setWindowed(int width, int height){
        Gdx.graphics.setWindowedMode(width, height);
    }

    public static void setWindowedFullscreen(){
        Gdx.graphics.setWindowedMode(Gdx.graphics.getDisplayMode().width, Gdx.graphics.getDisplayMode().height);
    }

    public static void setVSync(boolean vsync){
        Gdx.graphics.setVSync(vsync);
    }

}