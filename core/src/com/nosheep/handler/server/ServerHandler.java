package com.nosheep.handler.server;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.game.combatuser.Enemy;
import com.nosheep.game.TentaGame;
import com.nosheep.game.combatuser.Player;
import com.nosheep.model.skill.Skill;
import com.nosheep.util.GameUtil;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Johan on 2017-07-17.
 */
public class ServerHandler {

    //private String ADDRESS = "http://172.21.7.210:8080";
    private String ADDRESS = "http://localhost:8080";

    private Socket socket;

    private TentaGame game;

    public ServerHandler(){
        game = GameUtil.GAME;
        try{
            socket = IO.socket(ADDRESS);
            socket.connect();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public void Start(){
        configSocketEvents();
    }

    public void update(float delta){
        game.timer += delta;
        if(game.timer >= game.UPDATE_TIME && game.player != null && game.player.hasMoved()){
            JSONObject data = new JSONObject();
            try{
                data.put("x", game.player.getX());
                data.put("y", game.player.getY());
                data.put("direction", game.player.getDirection().ordinal());
                data.put("location", game.player.getLocation());
                socket.emit("playerMoved", data);
            }
            catch (JSONException e){
                Gdx.app.log("SOCKET.IO", "Error sending update data");
            }
        }
    }

    public void dealDamage(Enemy enemy, int damage) {
        JSONObject data = new JSONObject();
        try {
           data.put("id", enemy.getId());
           data.put("damage", damage);
           data.put("sender", game.player.getId());
           socket.emit("enemyDamaged", data);
        }
        catch(JSONException e) {
            e.printStackTrace();
        }
    }
    public void dealDamage(Player p) {

    }

    private void setUserName(){
        JSONObject data = new JSONObject();
        try{
            if(game.player.getUsername() == "" || game.player.getUsername() == null)
                data.put("username", game.player.getId());
            else
                data.put("username", game.player.getUsername());
            socket.emit("setUserName", data);
        }
        catch (JSONException e){
            Gdx.app.log("SOCKET.IO", "Error updating username");
        }
    }

    public void setLocation(){
        JSONObject data = new JSONObject();
        try{
            Gdx.app.log("setLocatoin: ", game.player.getLocation());
            data.put("location", game.player.getLocation());
            socket.emit("setLocation", data);
        }
        catch (JSONException e){
            Gdx.app.log("SOCKET.IO", "Error updating location");
        }
    }

    public void sendEnemyDied(Enemy enemy) {
        JSONObject data = new JSONObject();
        try{
            data.put("id", enemy.getId());
            socket.emit("enemyDied", data);
        }
        catch (JSONException e){
            Gdx.app.log("SOCKET.IO", "Error updating location");
        }
    }

    public void sendUsedSkill(Skill skill) {
        JSONObject data = new JSONObject();
        data.put("skillName", skill.getClass().getName());
        data.put("sender", skill.getSkillUser().getId());
        data.put("target", skill.getSkillTarget().getId());
        data.put("location", game.player.getLocation());
        socket.emit("usedSkill", data);
    }

    public void requestLocation(Player p) {
        JSONObject data = new JSONObject();
        try{
            data.put("id", p.getId());
            socket.emit("requestLocation", data);
        }
        catch(JSONException e) {}
    }

    private void configSocketEvents(){
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener(){
            @Override
            public void call(Object... args) {
                Gdx.app.log("SocketIO", "Connected");
                game.player = new Player(true);
            }
        }).on("socketID", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    game.player.setUsername(GameUtil.USERNAME);
                    setUserName();
                    game.player.setId(data.getString("id"));
                    game.player.setLocalPlayer(true);
                    setLocation();
                    Gdx.app.log("SocketIO", "My ID: " + game.player.getId());
                }
                catch(JSONException e){
                    Gdx.app.log("SocketIO", "Error getting ID");
                }
            }
        }).on("newPlayer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    String playerId = data.getString("id");
                    Gdx.app.log("SocketIO", "New Player Connected: " + playerId);
                    Player newPlayer = new Player(false);
                    newPlayer.setId(playerId);
                    game.playerList.put(playerId, newPlayer);
                }
                catch(JSONException e){
                    Gdx.app.log("SocketIO", "Error getting New PlayerID");
                }
            }
        }).on("playerDisconnected", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    String id = data.getString("id");
                    game.playerList.remove(id);
                }
                catch(JSONException e){
                    Gdx.app.log("SocketIO", "Error getting New PlayerID");
                }
            }
        }).on("getPlayers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray objects = (JSONArray) args[0];
                try{
                    for(int i = 0; i < objects.length(); i++){
                        //game.playerList.clear();
                        Player p = new Player(false);
                        Vector2 position = new Vector2();
                        position.x = ((Double) objects.getJSONObject(i).getDouble("x")).floatValue();
                        position.y = ((Double) objects.getJSONObject(i).getDouble("y")).floatValue();
                        p.setLocation(objects.getJSONObject(i).getString("location"));
                        p.setPosition(position.x, position.y);
                        p.setId(objects.getJSONObject(i).getString("id"));
                        p.setUsername(objects.getJSONObject(i).getString("username"));
                        game.playerList.put(p.getId(), p);
                    }
                }
                catch (JSONException e){

                }
            }
        }).on("getEnemies", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray objects = (JSONArray) args[0];
                List<Enemy> tempEnemyList = new ArrayList<Enemy>();
                try{
                    for(int i = 0; i < objects.length(); i++){
                        String classname = objects.getJSONObject(i).getString("classname");
                        String id = objects.getJSONObject(i).getString("id");
                        Boolean alive = objects.getJSONObject(i).getBoolean("alive");
                        int currentHealth = objects.getJSONObject(i).getInt("currentHealth");
                        Vector2 position = new Vector2();
                        position.x = ((Double) objects.getJSONObject(i).getDouble("x")).floatValue();
                        position.y = ((Double) objects.getJSONObject(i).getDouble("y")).floatValue();
                        boolean isInCombat = objects.getJSONObject(i).getBoolean("isInCombat");

                        try{
                            Enemy e = (Enemy) Class.forName("com.nosheep.model.enemy." + classname).newInstance();
                            e.setPositionX((int)position.x);
                            e.setPositionY((int)position.y);
                            e.setAlive(true);
                            e.setId(id);
                            e.setInCombat(isInCombat);
                            if(!alive) {
                                e.setCurrentHealth(0);
                                e.setAlive(false);
                                e.setInCombat(false);
                                e.setTarget(null);
                            }
                            else {
                                if(currentHealth > 0) e.setCurrentHealth(currentHealth);
                            }
                            tempEnemyList.add(e);
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    GameUtil.currentMap.enemies = tempEnemyList;
                }
                catch (JSONException e){
                    e.printStackTrace();
                }

                Gdx.app.log("getEnemies: ", "server sent out enemies. [" + objects.length() + "]");
            }
        }).on("playerMoved", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    String playerId = data.getString("id");
                    Double x = data.getDouble("x");
                    Double y = data.getDouble("y");
                    int direction = data.getInt("direction");

                    Player.DIRECTION dir = Player.DIRECTION.DOWN;

                    if(direction == 0)
                        dir = Player.DIRECTION.DOWN;
                    else if(direction == 1)
                        dir = Player.DIRECTION.UP;

                    if(game.playerList.get(playerId) != null){
                        game.playerList.get(playerId).setPosition(x.floatValue(), y.floatValue());
                        game.playerList.get(playerId).setDirection(dir);
                    }
                }
                catch(JSONException e){
                    Gdx.app.log("SocketIO", "Error getting playermovement");
                }
            }
        }).on("setUserName", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    String playerId = data.getString("id");
                    String username = data.getString("username");

                    if(game.playerList.get(playerId) != null){
                        game.playerList.get(playerId).setUsername(username);
                    }
                }
                catch(JSONException e){
                    Gdx.app.log("SocketIO", "Error getting player username");
                }
            }
        }).on("moveEnemy", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    int id = data.getInt("id");
                    int x = data.getInt("x");
                    int y = data.getInt("y");
                    Enemy enemy = GameUtil.getEnemyById(id);
                    if(enemy != null) {
                        enemy.move(x, y);
                    }
                }
                catch(NullPointerException e) {
                    Gdx.app.log("moveEnemy", e.getMessage());
                }
                catch(JSONException e){}
            }
        }).on("getRequestedPlayerLocation", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    String id = data.getString("id");
                    String location = data.getString("location");
                    for(Iterator<Map.Entry<String, Player>> it = game.playerList.entrySet().iterator(); it.hasNext(); ) {
                        Map.Entry<String, Player> entry = it.next();
                        Player p = entry.getValue();
                        if(p.getId().equals(id)) p.setLocation(location);
                    }
                }
                catch(JSONException e) {

                }
            }
        }).on("playerChangedLocation", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try{
                    Player p = new Player(false);
                    Vector2 position = new Vector2();
                    position.x = ((Double) data.getDouble("x")).floatValue();
                    position.y = ((Double) data.getDouble("y")).floatValue();
                    p.setLocation(data.getString("location"));
                    p.setPosition(position.x, position.y);
                    p.setId(data.getString("id"));
                    p.setUsername(data.getString("username"));
                    if(p.getLocation().equals(game.player.getLocation())) {
                        game.playerList.put(p.getId(), p);
                    }
                    else {
                        game.playerList.remove(p.getId());
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }).on("enemyDamaged", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                int damage = (Integer) args[1];
                try{
                    String id = data.getString("id");
                    for(Enemy e : GameUtil.currentMap.enemies) {
                        if(e.getId().equals(id)) {
                            e.dealDamage(false, damage);
                        }
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }).on("usedSkill", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    String skillName = data.getString("skillName");
                    String senderId = data.getString("sender");
                    String targetId = data.getString("target");
                    CombatUser sender = null;
                    CombatUser target = null;
                    List<CombatUser> combatUsers = new ArrayList<CombatUser>();
                    for(Iterator<Map.Entry<String, Player>> it = game.playerList.entrySet().iterator(); it.hasNext(); ) {
                        Map.Entry<String, Player> entry = it.next();
                        Player p = entry.getValue();
                        combatUsers.add(p);
                    }
                    for(Enemy e : GameUtil.currentMap.enemies) {
                        combatUsers.add(e);
                    }

                    for(CombatUser cu : combatUsers) {
                        if(sender != null && target != null) {
                            break;
                        }
                        if(cu.getId().equals(senderId)) sender = cu;
                        else if(cu.getId().equals(targetId)) target = cu;
                    }

                    if(sender != null && target != null && target.isAlive()) {
                        Skill skill = (Skill) Skill.class.forName(skillName).newInstance();
                        skill.setSkillUser(sender);
                        skill.setSkillTarget(target);
                        skill.init();
                        skill.setDealDamage(false);
                        sender.useSkill(skill);
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }).on("respawnEnemy", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String location = data.getString("location");
                if(location.equals(game.player.getLocation())) {
                    String classname = data.getString("classname");
                    String id = data.getString("id");
                    Boolean alive = data.getBoolean("alive");
                    int currentHealth = data.getInt("currentHealth");
                    Vector2 position = new Vector2();
                    position.x = ((Double) data.getDouble("x")).floatValue();
                    position.y = ((Double) data.getDouble("y")).floatValue();

                    try {
                        Enemy e = (Enemy) Class.forName("com.nosheep.model.enemy." + classname).newInstance();
                        e.setPositionX((int) position.x);
                        e.setPositionY((int) position.y);
                        e.setAlive(true);
                        e.setId(id);
                        GameUtil.currentMap.enemies.add(e);
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).on("enemyChoseTarget", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String targetID = (String) args[1];
                String location = data.getString("location");
                if(location.equals(game.player.getLocation())) {
                    String id = data.getString("id");
                    boolean isInCombat = data.getBoolean("isInCombat");
                    Player targetPlayer = null;
                    if(game.player.getId().equals(targetID)) {
                        targetPlayer = game.player;
                    }
                    else {
                        for(Iterator<Map.Entry<String, Player>> it = game.playerList.entrySet().iterator(); it.hasNext(); ) {
                            Map.Entry<String, Player> entry = it.next();
                            Player p = entry.getValue();
                            if(p.getId().equals(targetID)) {
                                targetPlayer = p;
                                break;
                            }
                        }
                    }
                    for(Enemy e : GameUtil.currentMap.enemies) {
                        if(e.getId().equals(id) && isInCombat && targetPlayer != null) {
                            e.setInCombat(isInCombat);
                            e.setTarget(targetPlayer);
                        }
                    }
                }
            }
        }).on("setEnemyCombatFalse", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString("id");
                for(Enemy e : GameUtil.currentMap.enemies) {
                    if(e.getId().equals(id)) {
                        e.setInCombat(false);
                        e.setTarget(null);
                        break;
                    }
                }
            }
        });
    }
}
