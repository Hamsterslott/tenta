package com.nosheep.handler.skill;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nosheep.model.skill.Skill;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class SkillHandler {

    public List<Skill> activeSkills;
    public List<Skill> unactiveSkills;

    public SkillHandler(){
        activeSkills = new ArrayList<Skill>();
        unactiveSkills = new ArrayList<Skill>();
    }

    public void add(Skill skill) {
        activeSkills.add(skill);
    }

    public void render(SpriteBatch batch, float delta) {
        for (Skill s : activeSkills) {
            if (s.isActive())
                s.render(batch, delta);
            else
                unactiveSkills.add(s);
        }
        removeUnactiveSkills();
    }

    private void removeUnactiveSkills() {
        activeSkills.removeAll(unactiveSkills);
    }

    public void useSkill(Skill skill) {
        activeSkills.add(skill);
    }

}
