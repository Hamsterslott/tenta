package com.nosheep.handler.skill;

import com.badlogic.gdx.Gdx;
import com.nosheep.game.combatuser.Player;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.model.skill.Skill;
import com.nosheep.tool.Timer;
import com.nosheep.util.ServerUtil;

public class AutoAttackHandler {

    private Skill skill;
    private Timer timer;
    private CombatUser combatUser;
    private boolean isAttacking = false;

    public AutoAttackHandler(CombatUser combatUser) {
        this.combatUser = combatUser;
        this.timer = new Timer();
    }

    public void startAttacking() {
        timer.start();
        isAttacking = true;
    }

    public void stopAttacking() {
        timer.reset();
        isAttacking = false;
    }

    public void update() {
        verifyTarget();

        if(timer.getCurrentTime() > skill.getCooldown() && isAttacking) {

                Skill tempSkill = createSkill();
                if(tempSkill == null) {
                    Gdx.app.log("AutoAttackHandler", "NULL");
                    return;
                }

                combatUser.getSkillHandler().useSkill(tempSkill);
                if(combatUser instanceof Player) {
                    Player p = (Player) combatUser;
                    if(p.isLocalPlayer())
                        ServerUtil.sendUsedSkill(tempSkill);
                }

            timer.reset();
        }
    }
    private void verifyTarget() {
        if(combatUser.getTarget() == null) {
            stopAttacking();
            return;
        }
        else
            skill.setSkillTarget(combatUser.getTarget());
    }
    private Skill createSkill() {
        try {
            Skill tempSkill = (Skill) Skill.class.forName(skill.getClass().getName()).newInstance();
            tempSkill.setSkillUser(combatUser);
            tempSkill.setSkillTarget(combatUser.getTarget());
            tempSkill.init();
            tempSkill.setDealDamage(true);

            return tempSkill;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public boolean isAttacking() {
        return isAttacking;
    }

}
