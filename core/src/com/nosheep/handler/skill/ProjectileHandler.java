package com.nosheep.handler.skill;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Enemy;
import com.nosheep.game.combatuser.Player;
import com.nosheep.model.skill.Skill;
import com.nosheep.util.GameUtil;
import com.nosheep.tool.Timer;

public class ProjectileHandler extends Skill {

    private boolean followTarget;
    protected Timer timer;
    private boolean hitMultipleTargetsOnImpact = false;

    public ProjectileHandler() {
        init();
    }

    @Override
    public void init() {
        timer = new Timer();
        timer.start();

        if(skillUser != null && skillTarget != null) {
            if(GameUtil.getPlayer().getDirection() == Player.DIRECTION.UP)
                setStartingX(skillUser.getX() + skillUser.getWidth() / 2 + 15);
            else
                setStartingX(skillUser.getX() - skillUser.getWidth() / 2 - 10);
            setStartingY(skillUser.getY() + 30);

            initPositionAndDestination();
            calculateTravelVector();

        }
    }

    private void initPositionAndDestination() {
        setPositionX(getStartingX());
        setPositionY(getStartingY() + getWidth() / 2);
        setDestinationX(skillTarget.getX());
        setDestinationY(skillTarget.getY());
    }
    private void calculateTravelVector() {
        Vector2 travelV = new Vector2(getDestinationX() - getStartingX(),
                getDestinationY() - getStartingY());
        float vectorLength = travelV.len();
        travelV.x = travelV.x / vectorLength;
        travelV.y = travelV.y / vectorLength;
        setTravelVector(travelV);
    }

    private void followTargetMove() {
        if(getPositionX() <= getSkillTarget().getX()) setPositionX(getPositionX() + getProjectileSpeed());
        else if(getPositionX() >= getSkillTarget().getX()) setPositionX(getPositionX() - getProjectileSpeed());

        if(getPositionY() <= getSkillTarget().getY()) setPositionY(getPositionY() + getProjectileSpeed());
        else if(getPositionY() >= getSkillTarget().getY()) setPositionY(getPositionY() - getProjectileSpeed());
    }

    private void linearMove() {
        Vector2 newPos = new Vector2(
                (getPositionX() + (getTravelVector().x) * getProjectileSpeed()),
                (getPositionY() + (getTravelVector().y) * getProjectileSpeed())
        );
        Vector2 distanceFromStart = calculateDistanceFromStart(newPos);
        fixProjectileDirection();

        if(distanceFromStart.len() >= getProjectileDistance()) {
            if(getHitAnimation() != null) {
                if(getHitAnimation().getAnimation().isAnimationFinished(getHitAnimation().getAnimation().getAnimationDuration())) {
                    setActive(false);
                }
            }
            setActive(false);
        }
        else {
            setPositionX(newPos.x);
            setPositionY(newPos.y);
        }
    }

    private void fixProjectileDirection() {
        boolean flipX = false;
        boolean flipY = false;
        if(getTravelVector().x < 0)
            flipX = true;
        if(getTravelVector().y < 0)
            flipY = true;
        if(isUseParticleEffect()) {
            GameUtil.changeEmitterAngle(getParticleEffect(),
                    getParticleEffect().getEmitters().first().getName(),
                    getTravelVector().angle());
            getParticleEffect().setFlip(flipX, flipY);
        } else {
            // TODO: Rotate animation
        }
    }

    private Vector2 calculateDistanceFromStart(Vector2 currentPos) {

        return new Vector2(currentPos.x - getStartingX(), currentPos.y - getStartingY());
    }

    private void checkActiveState() {
        /*if(!getSkillTarget().isAlive()) {
            setActive(false);
        }
        else */if(isCollidingWithAnEnemy()) {
            setActive(false);
        }
        // TODO: Logic for player pvp
    }

    private boolean isCollidingWithAnEnemy() {
        Rectangle skill = new Rectangle(getPositionX(), getPositionY(), getWidth(), getHeight());
        boolean hasHitEnemy = false;
        for(Enemy e : GameUtil.currentMap.enemies) {
            if(e.isAlive() && GameUtil.isOverlaping(skill, e.getBounds())) {
                if (isDealDamage())
                    e.dealDamage(true, getDamage() + getSkillUser().getPower());
                if(hitMultipleTargetsOnImpact)
                    hasHitEnemy = true;
                else
                    return true;
            }
        }
        return hasHitEnemy;
    }

    @Override
    protected void update() {
        super.update();
        checkActiveState();
        if(followTarget)
            followTargetMove();
        else
            linearMove();
    }

    @Override
    public void render(SpriteBatch batch, float delta) {
        super.render(batch, delta);
    }

    public boolean isFollowTarget() {
        return followTarget;
    }

    public void setFollowTarget(boolean followTarget) {
        this.followTarget = followTarget;
    }

    public void setHitMultipleTargetsOnImpact(boolean hitMultipleTargetsOnImpact) {
        this.hitMultipleTargetsOnImpact = hitMultipleTargetsOnImpact;
    }
}
