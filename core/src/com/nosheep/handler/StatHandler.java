package com.nosheep.handler;

import com.nosheep.game.combatuser.Player;
import com.nosheep.model.Statistic;

public class StatHandler {

    public Statistic stats;
    public Player player;

    public StatHandler() {
        initPlayerStats();
    }

    public void linkPlayer(Player player) {
        this.player = player;
    }

    public void updatePlayerStats() {
        if(stats.currentExperience >= stats.maxExperience) levelUpPlayer();
    }

    private void levelUpPlayer() {
        stats.level += 1;
        stats.currentExperience = stats.maxExperience - stats.currentExperience;
        stats.maxExperience = (stats.level * stats.level) * 100;
        stats.maxHealth += 20;
        stats.currentHealth = stats.maxHealth;
        stats.power += 5;
        stats.defence += 5;
    }

    private void initNewPlayerStats() {
        stats.level = 1;
        stats.currentExperience = 0;
        stats.maxExperience = (stats.level * stats.level) * 100;
        stats.maxHealth = 100;
        stats.currentHealth = stats.maxHealth;
        stats.movementSpeed = 200;
        stats.power = 5;
        stats.defence = 5;
    }

    private void initPlayerStats(){
       stats = new Statistic();
       initNewPlayerStats();
    }

    public int getTotalPower() {
        int pow = stats.power;
        if(player.getWeapon() != null) pow += player.getWeapon().getPower();
        if(player.getHelmet() != null) pow += player.getHelmet().getPower();
        if(player.getBody() != null) pow += player.getBody().getPower();
        if(player.getLegs() != null) pow += player.getLegs().getPower();
        if(player.getGloves() != null) pow += player.getGloves().getPower();
        if(player.getBoots() != null) pow += player.getBoots().getPower();
        if(player.getCape() != null) pow += player.getCape().getPower();
        return pow;
    }

    public int getTotalDefence() {
        int def = stats.defence;
        if(player.getWeapon() != null) def += player.getWeapon().getDefence();
        if(player.getHelmet() != null) def += player.getHelmet().getDefence();
        if(player.getBody() != null) def += player.getBody().getDefence();
        if(player.getLegs() != null) def += player.getLegs().getDefence();
        if(player.getGloves() != null) def += player.getGloves().getDefence();
        if(player.getBoots() != null) def += player.getBoots().getDefence();
        if(player.getCape() != null) def += player.getCape().getDefence();
        return def;
    }

    public float getTotalMaxHealth() {
        return stats.maxHealth;
    }

}
