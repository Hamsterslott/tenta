package com.nosheep.handler;

import com.nosheep.interfaces.Item;
import com.nosheep.model.inventory.Bag;
import com.nosheep.model.item.currency.Coins;

import java.util.List;

/**
 * Created by Johan on 2017-08-16.
 */
public class BagHandler {

    private Bag bag;
    private int coins = 0;

    public BagHandler(Bag bag) {
        this.bag = bag;
    }

    public boolean isFull(){
        if(bag.getItems().size() >= bag.getCapacity())
            return true;
        return false;
    }

    public void addItemToBag(Item item) {
        bag.addItem(item);
    }
    public void addCoinsToBag(Coins coins) {
        this.coins += coins.getLootAmount();
    }

    public List<Item> getItems() {
        return bag.getItems();
    }

    public int getCoins() {
        return coins;
    }
}
