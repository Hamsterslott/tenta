package com.nosheep.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import com.nosheep.game.asset.AssetLoader;
import com.nosheep.game.combatuser.Player;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;
import com.nosheep.screen.LoginScreen;

import java.util.HashMap;

public class TentaGame extends Game {

	public final float UPDATE_TIME = 1/60f;
	public float timer;

	String id;
	public Player player;
	public HashMap<String, Player> playerList;

	@Override
	public void create () {
		loadAssets();
		GraphicUtil.setWindowedFullscreen();
		GameUtil.GAME = this;
		playerList = new HashMap<String, Player>();
		//setScreen(new SplashScreen());
		setScreen(new LoginScreen());
	}

	private void loadAssets() {
		AssetUtil.ASSET_LOADER = new AssetLoader();
		Gdx.app.log("Initializing", ".");
		AssetUtil.ASSET_LOADER.load();
		Gdx.app.log("Initializing", "..");
		AssetUtil.ASSET_LOADER.manager.finishLoading();
		AssetUtil.ASSET_LOADER.applyFilters();
		Gdx.app.log("Initializing", "...");

		while(!AssetUtil.ASSET_LOADER.manager.update()) {
			Gdx.app.log("loading", (AssetUtil.ASSET_LOADER.manager.getProgress() * 100) + " %");
		}

		AssetUtil.ASSET_LOADER.initLists();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
		AssetUtil.ASSET_LOADER.dispose();
	}
}
