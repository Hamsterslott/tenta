package com.nosheep.game.combatuser;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.animation.AnimatedSprite;
import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.currency.Coins;
import com.nosheep.util.*;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.handler.skill.SkillHandler;
import com.nosheep.handler.StatHandler;
import com.nosheep.interfaces.Item;
import com.nosheep.model.DamageRenderPackage;
import com.nosheep.model.Statistic;
import com.nosheep.model.inventory.LootBox;
import com.nosheep.model.skill.Skill;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Johan on 2017-08-23.
 */
public class Enemy implements CombatUser {

    protected String id;
    private int positionX;
    private int positionY;
    protected int spawnX;
    protected int spawnY;
    private int width;
    private int height;
    private boolean localPlayerEarnedLootAndExperience = false;
    private boolean hasDroppedLoot = false;
    private int experience;
    private int maxCoinDrop = 0;

    private boolean alive;
    private boolean hasDied = false;
    private boolean inCombat;

    private String name;

    private CombatUser target;

    private AnimatedSprite idleAnimation;
    private AnimatedSprite deathAnimation;

    private Texture icon;

    protected List<Item> loot;

    public String location;

    private SkillHandler skillHandler;
    private Statistic stats;

    private BitmapFont smallFont;
    private GlyphLayout layout = new GlyphLayout();

    public Enemy(){
        alive = true;
        inCombat = false;
        loot = new ArrayList<Item>();
        skillHandler = new SkillHandler();
        smallFont = AssetUtil.getFont(Assets.fntNeuroPoliticalSmall);
        stats = new Statistic();
    }

    public void move(int x, int y){
        Rectangle r = idleAnimation.getBoundingRectangle();
        r.setX(x);
        r.setY(y);
        if(!GameUtil.isOverlapingMapObject(r)) {
            setPositionX(x);
            setPositionY(y);

            if(getPositionX() >= GraphicUtil.WIDTH - getWidth())
                setPositionX(GraphicUtil.WIDTH - getWidth());
            if(getPositionY() >= GraphicUtil.HEIGHT - getHeight())
                setPositionX(GraphicUtil.HEIGHT - getHeight());
            if(getPositionX() <= 0)
                setPositionX(0);
            if(getPositionY() <= 0)
                setPositionY(0);
        }
    }

    protected void dropLoot(){
        if(!hasDroppedLoot) {
            Random rand = new Random();
            if (!loot.isEmpty()) {
                int coinOrCash = Math.abs(rand.nextInt() % 10);
                if (coinOrCash < 8)
                    dropCoins();
                else
                    dropItems(rand);
                hasDroppedLoot = true;
            }
        }
    }

    private void dropItems(Random rand) {
        try {
            int random = Math.abs(rand.nextInt() % loot.size());
            LootBox lootBox = new LootBox();
            Item lootItem = loot.get(random);
            lootBox.setLootItem(lootItem.getClass().newInstance());
            lootBox.setLootPosition(new Vector2(getPositionX(), getPositionY()));
            GameUtil.currentMap.loot.add(lootBox);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ArithmeticException e) {

        }
    }

    private void dropCoins() {
        Coins coins = new Coins(maxCoinDrop);
        LootBox lootBox = new LootBox();
        lootBox.setLootItem(coins);
        lootBox.setLootPosition(new Vector2(getPositionX(), getPositionY()));
        GameUtil.currentMap.loot.add(lootBox);
    }

    public void update(float delta){
        if(alive) {
            checkIfPlayerTargetedMe();
        }

        if(stats.currentHealth <= 0 && !hasDied){
            stats.currentHealth = 0;
            alive = false;
            if(GameUtil.getPlayer().getTarget() != null &&
                GameUtil.getPlayer().getTarget().equals(this))
                    changePlayerTarget();

            if(localPlayerEarnedLootAndExperience) {
                dropLoot();
                GameUtil.getPlayer().addExperience(experience);
            }
            ServerUtil.sendEnemyDied(this);
            hasDied = true;
        }
    }
    private void changePlayerTarget() {
        boolean didSetTarget = false;
        for(Enemy e : GameUtil.currentMap.enemies) {
            if(e.equals(this)) continue;
            else if(e.getTarget() != null &&
                    e.getTarget().equals(GameUtil.getPlayer())) {
                GameUtil.getPlayer().setTarget(e);
                didSetTarget = true;
                break;
            }
        }
        if(!didSetTarget) GameUtil.getPlayer().setTarget(null);
    }
    private void checkIfPlayerTargetedMe(){
        Vector2 mouse = InputUtil.getMouseCoordinates();
        Rectangle r = new Rectangle(getPositionX(), getPositionY(), getWidth(), getHeight());
        if(InputUtil.isMouseLeftClickedOutsideWindow() && r.contains(mouse)) {
            GameUtil.setCombatUserTarget(this, GameUtil.getPlayer());
        }
    }

    private void renderSkills(SpriteBatch batch, float delta) {
        skillHandler.render(batch, delta);
    }

    public void render(SpriteBatch batch, ShapeRenderer shapeRenderer, float delta){
        update(delta);
        if(alive) {
            shapeRenderer.begin();
            if(GameUtil.getPlayer().getTarget() == this) {
                if(isInCombat() && target == GameUtil.getPlayer()) shapeRenderer.setColor(Color.RED);
                else if (isInCombat()) shapeRenderer.setColor(Color.YELLOW);
                else shapeRenderer.setColor(Color.WHITE);
                shapeRenderer.rect(getPositionX(), getPositionY(), getWidth(), getHeight());
            }
            shapeRenderer.end();

            batch.begin();
            if(idleAnimation != null) idleAnimation.drawAnimatedSprite(batch, getWidth(), getHeight());
            renderNameAndLevel(batch);
            renderHealthBar(batch);
            batch.end();
        }
        else {
            batch.begin();
            if(deathAnimation != null) deathAnimation.drawAnimatedSprite(batch, getWidth(), getHeight());
            batch.end();
        }
    }
    private void renderNameAndLevel(SpriteBatch batch){
        layout.setText(smallFont, stats.level + ". " + name);
        smallFont.setColor(Color.WHITE);
        smallFont.draw(batch, layout, (getPositionX() + getWidth() / 2) - layout.width / 2, getPositionY() + getHeight() + 35);
    }
    private void renderHealthBar(SpriteBatch batch) {
        float healthPercentage = (stats.currentHealth / stats.maxHealth) * 100;
        if(healthPercentage > 70) batch.draw(AssetUtil.getTexture(Assets.healthBarGreen), (getPositionX() + getWidth() / 2) - 50, getPositionY() + getHeight() + 5, healthPercentage, 15);
        else if(healthPercentage > 30) batch.draw(AssetUtil.getTexture(Assets.healthBarYellow), (getPositionX() + getWidth() / 2) - 50, getPositionY() + getHeight() + 5, healthPercentage, 15);
        else batch.draw(AssetUtil.getTexture(Assets.healthBarRed), (getPositionX() + getWidth() / 2) - 50, getPositionY() + getHeight() + 5, healthPercentage, 15);
    }

    @Override
    public void dealDamage(boolean updateServer, int damage) {
        if(updateServer) {
            int reduceDamage = getDefence() / 2 + (int)(Math.random() * getDefence());
            int dmg = damage - reduceDamage;
            if(dmg < 0) dmg = 0;
            stats.currentHealth -= dmg;
            ServerUtil.SERVER_HANDLER.dealDamage(this, dmg);
            this.localPlayerEarnedLootAndExperience = true;
            displayDamage(dmg);
        } else {
            stats.currentHealth -= damage;
            displayDamage(damage);
        }

    }
    private void displayDamage(int damage) {
        Random rn = new Random();
        int offset = 50;
        int currentStartX = this.getX() + this.getWidth() / 2;
        int min = currentStartX - offset;
        int max = currentStartX + offset;
        int range = max - min + 1;
        int randomX =  rn.nextInt(range) + min;
        DamageRenderPackage drp = new DamageRenderPackage();
        drp.setDamage(damage);
        drp.setStartPosition(new Vector2(randomX, this.getY() + this.getHeight() / 2));
        GameUtil.currentMap.damageToRender.add(drp);
    }

    /////////////////////////
    // GETTERS AND SETTERS //
    /////////////////////////


    public void setMaxCoinDrop(int maxCoinDrop) {
        this.maxCoinDrop = maxCoinDrop;
    }

    @Override
    public void useSkill(Skill skill) {
        skillHandler.useSkill(skill);
    }

    @Override
    public StatHandler getStatHandler() {
        return null;
    }

    @Override
    public SkillHandler getSkillHandler() {
        return skillHandler;
    }

    @Override
    public Statistic getStats() {
        return stats;
    }

    @Override
    public CombatUser getTarget() {
        return this.target;
    }

    @Override
    public boolean hasTarget() {
        if(this.target != null)
            return true;
        return false;
    }

    @Override
    public void setTarget(CombatUser combatUser) {
        this.target = combatUser;
    }

    @Override
    public void setIcon(Texture icon) {
        this.icon = icon;
    }

    @Override
    public Texture getIcon() {
        return icon;
    }

    public AnimatedSprite getIdleAnimation() {
        return idleAnimation;
    }

    public void setIdleAnimation(AnimatedSprite idleAnimation) {
        this.idleAnimation = idleAnimation;
        this.idleAnimation.setPosition(positionX, positionY);
        this.idleAnimation.setSize(width, height);
    }

    public AnimatedSprite getDeathAnimation() {
        return deathAnimation;
    }

    public void setDeathAnimation(AnimatedSprite deathAnimation) {
        this.deathAnimation = deathAnimation;
        this.deathAnimation.setPosition(positionX, positionY);
        this.deathAnimation.setSize(width, height);
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
        this.idleAnimation.setPosition(positionX, positionY);
        this.deathAnimation.setPosition(positionX, positionY);
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
        this.idleAnimation.setPosition(positionX, positionY);
        this.deathAnimation.setPosition(positionX, positionY);
    }

    @Override
    public int getX() {
        return this.positionX;
    }

    @Override
    public int getY() {
        return this.positionY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isInCombat() {
        return inCombat;
    }

    public void setInCombat(boolean inCombat) {
        this.inCombat = inCombat;
    }

    public int getLvl() {
        return stats.level;
    }

    public void setLvl(int lvl) {
        stats.level = lvl;
        experience = (lvl * lvl) * 4;
    }

    public int getPower() {
        return stats.power;
    }

    public void setPower(int power) {
        stats.power = power;
    }

    public int getDefence() {
        return stats.defence;
    }

    public void setDefence(int defence) {
        stats.defence = defence;
    }

    public float getCurrentHealth() {
        return stats.currentHealth;
    }

    @Override
    public float getMaximumHealth() {
        return stats.maxHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        stats.currentHealth = currentHealth;
    }

    public void setMaxHealth(int maxHealth) {
        stats.maxHealth = maxHealth;
        setCurrentHealth(maxHealth);
    }

    public List<Item> getLoot() {
        return loot;
    }

    public void setLoot(List<Item> loot) {
        this.loot = loot;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rectangle getBounds() {
        return idleAnimation.getBoundingRectangle();
    }

}
