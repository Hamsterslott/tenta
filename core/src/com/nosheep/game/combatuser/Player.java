package com.nosheep.game.combatuser;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.handler.*;
import com.nosheep.handler.skill.AutoAttackHandler;
import com.nosheep.handler.skill.SkillHandler;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.game.asset.Assets;
import com.nosheep.interfaces.Item;
import com.nosheep.model.inventory.SmallBag;
import com.nosheep.model.item.currency.Coins;
import com.nosheep.model.item.equipment.*;
import com.nosheep.model.item.equipment.body.IronBody;
import com.nosheep.model.item.equipment.boots.BeginnerBoots;
import com.nosheep.model.item.equipment.capes.BeginnerCape;
import com.nosheep.model.item.equipment.gloves.BeginnerGloves;
import com.nosheep.model.item.equipment.helmet.IronHelmet;
import com.nosheep.model.item.equipment.legs.IronLegs;
import com.nosheep.model.Statistic;
import com.nosheep.model.item.weapon.Weapon;
import com.nosheep.model.item.weapon.staff.BeginnerStaff;
import com.nosheep.model.skill.Arrow;
import com.nosheep.model.skill.Fireball;
import com.nosheep.model.skill.Skill;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;
import com.nosheep.util.InputUtil;

import java.util.List;
import java.util.Random;

/**
 * Created by Johan on 2017-06-29.
 */

public class Player implements CombatUser {

    public enum DIRECTION{
        DOWN,
        UP,
        LEFT,
        RIGHT
    };
    private String id = "";
    private String username;
    private boolean isLocalPlayer = false;
    private boolean alive = true;
    private String location;

    private CombatUser target;
    private Vector2 previousPosition;
    private DIRECTION direction;
    private Sprite sprite;
    private Texture[] head;

    private Weapon weapon;
    private Helmet helmet;
    private Body body;
    private Legs legs;
    private Gloves gloves;
    private Boots boots;
    private Cape cape;
    private BagHandler bagHandler;

    private SkillHandler skillHandler;
    private AutoAttackHandler autoAttackHandler;

    private StatHandler statHandler;

    private int customUserTabIndex = 0;

    // TEMPORARY testing, still working on graphics and sizes
    private int width = 42;
    private int height = 80;
    private BitmapFont smallFont;
    private GlyphLayout layout = new GlyphLayout();

    public Player(boolean isLocalPlayer) {
        this.isLocalPlayer = isLocalPlayer;
        if(location == null && isLocalPlayer) location = GameUtil.currentMap.getClass().getSimpleName();
        if(isLocalPlayer) initLocalPlayer();
        else initConnectedPlayer();

        sprite.setSize(width, height);
        previousPosition = new Vector2(getX(), getY());
        smallFont = AssetUtil.getFont(Assets.fntNeuroPoliticalSmall);
        try {
            setPosition(GameUtil.currentMap.startingPoint.x, GameUtil.currentMap.startingPoint.y);
        } catch(NullPointerException e) {
            setPosition(500, 500);
        }

        direction = DIRECTION.UP;

        bagHandler = new BagHandler(new SmallBag());
        skillHandler = new SkillHandler();
        statHandler = new StatHandler();
        statHandler.linkPlayer(this);

        autoAttackHandler = new AutoAttackHandler(this);
    }

    private void initLocalPlayer(){
        sprite = new Sprite(AssetUtil.getTexture(Assets.tempLocalPlayer));
        head = Assets.getHeadByName("head1");
        weapon = (Weapon) GameUtil.getItemByName(BeginnerStaff.class.getSimpleName());
        helmet = new IronHelmet();
        body = new IronBody();
        legs = new IronLegs();
        gloves = new BeginnerGloves();
        boots = new BeginnerBoots();
        cape = new BeginnerCape();
    }
    private void initConnectedPlayer(){
        sprite = new Sprite(AssetUtil.getTexture(Assets.tempConnectedPlayer));
        head = Assets.getHeadByName("head1");
        helmet = new IronHelmet();
        body = new IronBody();
        legs = new IronLegs();
        gloves = new BeginnerGloves();
        boots = new BeginnerBoots();
        cape = new BeginnerCape();
        weapon = new BeginnerStaff();
    }

    private void handleInput(float delta){
        handleAutoAttack();
        handleMovementInput(delta);
        handleRightClick();
        handleTabbingEnemyTargets();
    }
    private void handleAutoAttack() {
        if(!autoAttackHandler.isAttacking()
                && hasTarget()
                && Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)
                && hasTarget()) {
                autoAttackHandler.startAttacking();
        }
    }
    private void handleMovementInput(float delta){
        // Hardcoded, replace with KeyBindings
        if(Gdx.input.isKeyPressed(Input.Keys.A)
                || Gdx.input.isKeyPressed(Input.Keys.LEFT)) moveLeft(delta);
        else if(Gdx.input.isKeyPressed(Input.Keys.D)
                || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) moveRight(delta);

        if(Gdx.input.isKeyPressed(Input.Keys.W)
                || Gdx.input.isKeyPressed(Input.Keys.UP)) moveUp(delta);
        else if(Gdx.input.isKeyPressed(Input.Keys.S)
                || Gdx.input.isKeyPressed(Input.Keys.DOWN)) moveDown(delta);
    }
    private void handleRightClick() {
        if(InputUtil.isMouseRightClickedOutsideWindow()) this.target = null;
    }
    private void handleTabbingEnemyTargets(){
        int enemyListSize = GameUtil.currentMap.enemies.size();
        if(Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
            Random r = new Random();
            boolean foundTarget = false;
            List<CombatUser> combatUsersAlive;
            try {
                for (int i = 0; i < enemyListSize; i++) {
                    if (GameUtil.currentMap.enemies.isEmpty()) break;
                    CombatUser cu = GameUtil.currentMap.enemies.get(customUserTabIndex);
                    if (cu.equals(this.target) || !cu.isAlive()) continue;
                    this.target = cu;
                }
            } catch(IndexOutOfBoundsException e) { } // No need for panic
        }
        customUserTabIndex += 1;
        if(customUserTabIndex >= enemyListSize) customUserTabIndex = 0;
    }

    public void useSkill(Skill skill) {
        skillHandler.useSkill(skill);
    }

    private void update(float delta){
        if(isLocalPlayer){
            setAutoAttack();
            autoAttackHandler.update();
            handleInput(delta);
            statHandler.updatePlayerStats();
        }
    }

    private void setAutoAttack() {
        if(weapon == null) {
            autoAttackHandler.setSkill(new Fireball());
        } else if(weapon.getWeaponType().equals(Weapon.WeaponType.STAFF)) {
            autoAttackHandler.setSkill(new Fireball());
        } else if(weapon.getWeaponType().equals(Weapon.WeaponType.BOW)) {
            autoAttackHandler.setSkill(new Arrow());
        } else if(weapon.getWeaponType().equals(Weapon.WeaponType.SWORD)) {
            autoAttackHandler.setSkill(new Fireball());
        }
    }

    private void renderSkills(SpriteBatch batch, float delta){
        skillHandler.render(batch, delta);
    }

    public void render(SpriteBatch batch, float delta){
        update(delta);
        batch.begin();

        if(direction == DIRECTION.DOWN) renderDirectionDown(batch);
        else if(direction == DIRECTION.UP) renderDirectionUp(batch);

        renderNameAndLevel(batch);
        renderHealthBar(batch);
        renderSkills(batch, delta);

        batch.end();
    }

    private void renderNameAndLevel(SpriteBatch batch){
        String tempName;
        if(username == null) tempName = id;
        else tempName = username;
        layout.setText(smallFont, getStats().level + ". " + tempName);
        smallFont.setColor(Color.WHITE);
        smallFont.draw(batch, layout, (getX() + getWidth() / 2) - layout.width / 2, getY() + getHeight() + 35);
    }

    private void renderHealthBar(SpriteBatch batch) {
        float healthPercentage = (getStats().currentHealth / statHandler.getTotalMaxHealth()) * 100;
        if(healthPercentage > 70) batch.draw(AssetUtil.getTexture(Assets.healthBarGreen), (getX() + getWidth() / 2) - 50, getY() + getHeight() + 5, healthPercentage, 15);
        else if(healthPercentage > 30) batch.draw(AssetUtil.getTexture(Assets.healthBarYellow), (getX() + getWidth() / 2) - 50, getY() + getHeight() + 5, healthPercentage, 15);
        else batch.draw(AssetUtil.getTexture(Assets.healthBarRed), (getX() + getWidth() / 2) - 50, getY() + getHeight() + 5, healthPercentage, 15);
    }

    private void renderDirectionDown(SpriteBatch batch) {
        if (cape != null) cape.render(batch, direction, new Vector2(getX(), getY()));
        if(body != null) body.render(batch, direction, new Vector2(getX(), getY()));
        if(legs != null) legs.render(batch, direction, new Vector2(getX(), getY()));
        if(gloves != null) gloves.render(batch, direction, new Vector2(getX(), getY()));
        if(boots != null) boots.render(batch, direction, new Vector2(getX(), getY()));
        if(head != null) batch.draw(head[direction.ordinal()], getX() + getWidth() / 2 - 10, getY() + getHeight() - 20, 18, 18);
        if(helmet != null) helmet.render(batch, direction, new Vector2(getX(), getY()));
        if(weapon != null) weapon.render(batch, direction, new Vector2(getX() - getWidth() / 2 + 2, getY() + 10));
    }
    private void renderDirectionUp(SpriteBatch batch){
        if(weapon != null) weapon.render(batch, direction, new Vector2(getX() + getWidth() - weapon.getWidth() / 2, getY() + 10));
        if(head != null) batch.draw(head[direction.ordinal()], getX() + getWidth() / 2 - 10, getY() + getHeight() - 20, 18, 18);
        if(gloves != null) gloves.render(batch, direction, new Vector2(getX(), getY()));
        if(boots != null) boots.render(batch, direction, new Vector2(getX(), getY()));
        if(body != null) body.render(batch, direction, new Vector2(getX(), getY()));
        if(legs != null) legs.render(batch, direction, new Vector2(getX(), getY()));
        if (cape != null) cape.render(batch, direction, new Vector2(getX(), getY()));
        if(helmet != null) helmet.render(batch, direction, new Vector2(getX(), getY()));
    }

    private boolean isPlayerInsideOfMapBounds(Rectangle r){
        if(r.getX() >= GraphicUtil.WIDTH - getWidth())
            return false;
        if(r.getY() >= GraphicUtil.HEIGHT - getHeight())
            return false;
        if(r.getX() <= 0)
            return false;
        if(r.getY() <= 0)
            return false;

        return true;
    }

    public void addExperience(int experience) {
        getStats().currentExperience += experience;
    }

    public void moveLeft(float delta){
        //direction = DIRECTION.LEFT;
        Rectangle r = sprite.getBoundingRectangle();
        r.setX(getX() + (-getStats().movementSpeed) * delta);
        if(!GameUtil.isOverlapingMapObject(r) && isPlayerInsideOfMapBounds(r))
            setPosition(getX() + (-getStats().movementSpeed * delta), getY());
    }

    public void moveRight(float delta){
        //direction = DIRECTION.RIGHT;
        Rectangle r = sprite.getBoundingRectangle();
        r.setX(getX() + (+getStats().movementSpeed) * delta);
        if(!GameUtil.isOverlapingMapObject(r) && isPlayerInsideOfMapBounds(r))
            setPosition(getX() + (+getStats().movementSpeed * delta), getY());
    }

    public void moveUp(float delta){
        direction = DIRECTION.UP;
        Rectangle r = sprite.getBoundingRectangle();
        r.setY(getY() + (+getStats().movementSpeed) * delta);
        if(!GameUtil.isOverlapingMapObject(r) && isPlayerInsideOfMapBounds(r))
            setPosition(getX(), sprite.getY() + (+getStats().movementSpeed * delta));
    }

    public void moveDown(float delta){
        direction = DIRECTION.DOWN;
        Rectangle r = sprite.getBoundingRectangle();
        r.setY(getY() + (-getStats().movementSpeed) * delta);
        if(!GameUtil.isOverlapingMapObject(r) && isPlayerInsideOfMapBounds(r))
            setPosition(getX(), getY() + (-getStats().movementSpeed * delta));
    }

    public boolean hasMoved(){
        if(previousPosition.x != sprite.getX() || previousPosition.y != sprite.getY()){
            previousPosition.x = sprite.getX();
            previousPosition.y = sprite.getY();
            return true;
        }
        return false;
    }

    public void addItemToBag(Item item) {
        bagHandler.addItemToBag(item);
    }
    public void addCoinsToBag(Coins coins) {
        bagHandler.addCoinsToBag(coins);
    }

    @Override
    public StatHandler getStatHandler() {
        return statHandler;
    }

    @Override
    public Statistic getStats() {
        return statHandler.stats;
    }

    @Override
    public int getPower() {
        return statHandler.getTotalPower();
    }

    @Override
    public int getDefence() {
        return statHandler.getTotalDefence();
    }

    @Override
    public CombatUser getTarget() {
        return this.target;
    }

    @Override
    public boolean hasTarget() {
        if(this.target != null)
            return true;
        return false;
    }

    @Override
    public void setTarget(CombatUser combatUser) {
        this.target = combatUser;
    }

    @Override
    public Texture getIcon() {
        return helmet.textures[0];
    }

    @Override
    public void setIcon(Texture icon) {

    }

    @Override
    public void dealDamage(boolean updateServer, int damage) {
        this.getStats().currentHealth -= damage;
        if(updateServer) {
            // TODO: update server
        }
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    public float getCurrentExperience() {
        return getStats().currentExperience;
    }

    public void setCurrentExperience(float currentExperience) {
        getStats().currentExperience = currentExperience;
    }

    public float getMaxExperience() {
        return getStats().maxExperience;
    }

    public void setMaxExperience(float maxExperience) {
        getStats().maxExperience = maxExperience;
    }

    @Override
    public String getId() {
        return id;
    }

    public int getX(){
        return (int)sprite.getX();
    }

    public int getY(){
        return (int)sprite.getY();
    }

    public int getWidth(){
        return (int)sprite.getWidth();
    }

    public int getHeight(){
        return (int)sprite.getHeight();
    }

    @Override
    public String getName() {
        return this.username;
    }

    @Override
    public String getLocation() {
        return this.location;
    }

    @Override
    public float getCurrentHealth() {
        return getStats().currentHealth;
    }

    @Override
    public float getMaximumHealth() {
        return statHandler.getTotalMaxHealth();
    }

    @Override
    public int getLvl() {
        return getStats().level;
    }

    public void setPosition(float positionX, float positionY){
        sprite.setPosition(positionX, positionY);
    }

    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isLocalPlayer() {
        return isLocalPlayer;
    }

    public void setLocalPlayer(boolean localPlayer) {
        isLocalPlayer = localPlayer;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Vector2 getPreviousPosition() {
        return previousPosition;
    }

    public void setPreviousPosition(Vector2 previousPosition) {
        this.previousPosition = previousPosition;
    }

    public DIRECTION getDirection() {
        return direction;
    }

    public void setDirection(DIRECTION direction) {
        this.direction = direction;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public Texture[] getHead() {
        return head;
    }

    public void setHead(Texture[] head) {
        this.head = head;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Helmet getHelmet() {
        return helmet;
    }

    public void setHelmet(Helmet helmet) {
        this.helmet = helmet;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Legs getLegs() {
        return legs;
    }

    public void setLegs(Legs legs) {
        this.legs = legs;
    }

    public Gloves getGloves() {
        return gloves;
    }

    public void setGloves(Gloves gloves) {
        this.gloves = gloves;
    }

    public Boots getBoots() {
        return boots;
    }

    public void setBoots(Boots boots) {
        this.boots = boots;
    }

    public Cape getCape() {
        return cape;
    }

    public void setCape(Cape cape) {
        this.cape = cape;
    }

    public BagHandler getBagHandler() {
        return bagHandler;
    }

    public SkillHandler getSkillHandler() {
        return skillHandler;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
