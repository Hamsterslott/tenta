package com.nosheep.game.asset;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Johan on 2017-07-17.
 */
public class Assets {

    // TEXTURES BEGIN //
    public static final String splashTexture = "splashScreen.png";
    public static final String loginScreenBG = "loginScreenBG.png";
    public static final String loginLogo = "loginLogo.png";
    public static final String icon = "icon.png";
    public static final String healthBarGreen = "health/healthbar_green.png";
    public static final String healthBarYellow = "health/healthbar_yellow.png";
    public static final String healthBarRed = "health/healthbar_red.png";

    // PARTICLE EFFECTS //
    public static final String particleEffectDir = "particle/";
    public static final String fireballParticleEffect = "particle/fireball.pf";

    // TEMP //
    public static final String tempConnectedPlayer = "tempChar/tempConnectedPlayer.png";
    public static final String tempLocalPlayer = "tempChar/tempLocalPlayer.png";
    // ICONS //
    public static final String defaultLootIcon = "icons/defaultLootIcon.png";
    // ICONS //

    // CombatUserIcons //
    public static final String icon_frog = "combatUserIcon/icon_frog.png";
    public static final String icon_bat = "combatUserIcon/icon_bat.png";
    // CombatUserIcons //

    // UI //
    public static final String targetbg = "ui/targetbg.png";
    public static final String experienceBarBackground = "ui/experienceBarBackground.png";
    public static final String window_background = "ui/window_background.png";
    public static final String window_close = "ui/window_close.png";
    public static final String window_top_bar = "ui/window_top_bar.png";
    // UI //

    // HEAD //
    public static final String head1_back = "head/head1/back.png";
    public static final String head1_front = "head/head1/front.png";
    ///
    public static final String head2_back = "head/head2/back.png";
    public static final String head2_front = "head/head2/front.png";

    // ITEMS //
    public static final String coins = "items/coin/coins.png";

    public static final String ironHelmet_old_up = "character/back/helmet/ironhelmet_old.png";
    public static final String ironHelmet_old_down = "character/front/helmet/ironhelmet_old.png";

    public static final String ironHelmet_up = "character/back/helmet/ironhelmet.png";
    public static final String ironHelmet_down = "character/front/helmet/ironhelmet.png";
    public static final String ironHelmet_left = "character/left/helmet/ironhelmet.png";
    public static final String ironHelmet_right = "character/right/helmet/ironhelmet.png";

    public static final String ironBody_front = "character/front/body/ironbody.png";
    public static final String ironBody_back = "character/back/body/ironbody.png";
    public static final String ironLegs_front = "character/front/legs/ironlegs.png";
    public static final String ironLegs_back = "character/back/legs/ironlegs.png";
    public static final String beginnerBoots_front = "character/front/boots/beginnerboots.png";
    public static final String beginnerBoots_back = "character/back/boots/beginnerboots.png";
    public static final String beginnerGloves_front = "character/front/gloves/beginnergloves.png";
    public static final String beginnerGloves_back = "character/back/gloves/beginnergloves.png";
    public static final String beginnerCape_up = "items/cape/beginnercape_up.png";
    public static final String beginnerCape_down = "items/cape/beginnercape_down.png";

    // WEAPONS //
    public static final String beginnerStaff_up = "weapon/staff/beginnerstaff_up.png";
    public static final String beginnerStaff_down = "weapon/staff/beginnerstaff_down.png";
    public static final String beginnerBow_up = "weapon/bow/beginnerbow_up.png";
    public static final String beginnerBow_down = "weapon/bow/beginnerbow_down.png";

    // MAPS //
    public static final String tutorialCave01texture = "textureMap/TutorialCave01.png";
    public static final String batCavetexture = "textureMap/BatCave.png";

    // TEXTURES END //

    // SKIN //
    public static final String uiskin = "skin/uiskin.json";
    public static final String uiskinAtlas = "skin/uiskin.atlas";

    // AnimatedSprites / Pack
    public static final String frog_idle = "pack/idle_frog.pack";
    public static final String bat_idle = "pack/idle_bat.atlas";
    public static final String death_anim_gravestone = "pack/grave.pack";
    public static final String fireball_idle = "pack/fireball.atlas";
    public static final String arrow_idle = "pack/arrow.atlas";

    // FONTS //
    public static final String fntNeuroPolitical = "fonts/neuropolitical.fnt";
    public static final String fntNeuroPoliticalSmall = "fonts/neuropolitical_small.fnt";
    public static final String fntArialMedium = "fonts/arial_medium.fnt";
    public static final String fntArialMediumGray = "fonts/arial_medium_gray.fnt";

    // SOUNDS //
    public static final String hover = "sounds/hover.wav";
    public static final String click = "sounds/click.wav";

    // ARRAY //
    public static HashMap<String, Texture[]> heads = new HashMap<String, Texture[]>();
    public static HashMap<String, Texture[]> itemTextures = new HashMap<String, Texture[]>();

    public static Texture[] getHeadByName(String name){ return heads.get(name); }
    public static Texture[] getItemTextureByName(String name){
        return itemTextures.get(name);
    }
}
