package com.nosheep.game.asset;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.nosheep.game.asset.animation.AnimatedSprite;
import com.nosheep.interfaces.Item;
import com.nosheep.model.item.equipment.body.IronBody;
import com.nosheep.model.item.equipment.boots.BeginnerBoots;
import com.nosheep.model.item.equipment.capes.BeginnerCape;
import com.nosheep.model.item.equipment.gloves.BeginnerGloves;
import com.nosheep.model.item.equipment.helmet.IronHelmet;
import com.nosheep.model.item.equipment.legs.IronLegs;
import com.nosheep.model.item.weapon.bow.BeginnerBow;
import com.nosheep.model.item.weapon.staff.BeginnerStaff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Johan on 2017-09-21.
 */
public class AssetLoader {

    public static final AssetManager manager = new AssetManager();
    public static List<Item> itemList = new ArrayList<Item>();

    public void load() {
        loadImages();
        loadAnimations();
        loadSounds();
        loadSkins();
        loadFonts();
        loadHeads();
        loadItems();
        loadParticleEffects();
    }

    public void applyFilters() {
        Gdx.app.log("TextureFilters: ", "Applying filters..");
        Array<Texture> textureArray = manager.getAll(Texture.class, new Array<Texture>());
        for(Texture t : textureArray) {
            t.getTextureData().useMipMaps();
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.MipMapLinearLinear);
        }
        Array<BitmapFont> bitmapFontArray = manager.getAll(BitmapFont.class, new Array<BitmapFont>());
        for(BitmapFont bmf : bitmapFontArray) {
            bmf.getRegion().getTexture().getTextureData().useMipMaps();
            bmf.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.MipMapLinearLinear);
        }
    }

    private void loadImages() {
        manager.load(Assets.splashTexture, Texture.class);
        manager.load(Assets.loginScreenBG, Texture.class);
        manager.load(Assets.loginLogo, Texture.class);
        manager.load(Assets.icon, Texture.class);
        manager.load(Assets.healthBarGreen, Texture.class);
        manager.load(Assets.healthBarYellow, Texture.class);
        manager.load(Assets.healthBarRed, Texture.class);
        manager.load(Assets.tempConnectedPlayer, Texture.class);
        manager.load(Assets.tempLocalPlayer, Texture.class);
        manager.load(Assets.defaultLootIcon, Texture.class);
        manager.load(Assets.tutorialCave01texture, Texture.class);
        manager.load(Assets.batCavetexture, Texture.class);
        manager.load(Assets.icon_frog, Texture.class);
        manager.load(Assets.icon_bat, Texture.class);
        manager.load(Assets.targetbg, Texture.class);
        manager.load(Assets.experienceBarBackground, Texture.class);
        manager.load(Assets.window_background, Texture.class);
        manager.load(Assets.window_close, Texture.class);
        manager.load(Assets.window_top_bar, Texture.class);
    }
    private void loadAnimations(){
        manager.load(Assets.frog_idle, TextureAtlas.class);
        manager.load(Assets.bat_idle, TextureAtlas.class);
        manager.load(Assets.death_anim_gravestone, TextureAtlas.class);
        manager.load(Assets.fireball_idle, TextureAtlas.class);
        manager.load(Assets.arrow_idle, TextureAtlas.class);
    }
    private void loadSounds() {
        manager.load(Assets.hover, Sound.class);
        manager.load(Assets.click, Sound.class);
    }
    private void loadSkins() {
        SkinLoader.SkinParameter params = new SkinLoader.SkinParameter(Assets.uiskinAtlas);
        manager.load(Assets.uiskin, Skin.class, params);
    }
    private void loadFonts() {
        manager.load(Assets.fntNeuroPolitical, BitmapFont.class);
        manager.load(Assets.fntNeuroPoliticalSmall, BitmapFont.class);
        manager.load(Assets.fntArialMedium, BitmapFont.class);
        manager.load(Assets.fntArialMediumGray, BitmapFont.class);
    }

    private void loadHeads() {
        manager.load(Assets.head1_back, Texture.class);
        manager.load(Assets.head1_front, Texture.class);
        manager.load(Assets.head2_back, Texture.class);
        manager.load(Assets.head2_front, Texture.class);
    }

    private void loadItems(){
        manager.load(Assets.coins, Texture.class);
        manager.load(Assets.ironHelmet_old_up, Texture.class);
        manager.load(Assets.ironHelmet_old_down, Texture.class);
        manager.load(Assets.ironBody_front, Texture.class);
        manager.load(Assets.ironBody_back, Texture.class);
        manager.load(Assets.ironLegs_front, Texture.class);
        manager.load(Assets.ironLegs_back, Texture.class);
        manager.load(Assets.beginnerBoots_front, Texture.class);
        manager.load(Assets.beginnerBoots_back, Texture.class);
        manager.load(Assets.beginnerGloves_front, Texture.class);
        manager.load(Assets.beginnerGloves_back, Texture.class);
        manager.load(Assets.beginnerCape_up, Texture.class);
        manager.load(Assets.beginnerCape_down, Texture.class);
        manager.load(Assets.beginnerStaff_up, Texture.class);
        manager.load(Assets.beginnerStaff_down, Texture.class);
        manager.load(Assets.beginnerBow_up, Texture.class);
        manager.load(Assets.beginnerBow_down, Texture.class);
    }

    private void loadParticleEffects() {
        ParticleEffectLoader.ParticleEffectParameter pep = new ParticleEffectLoader.ParticleEffectParameter();
        pep.imagesDir = Gdx.files.internal(Assets.particleEffectDir);
        manager.load(Assets.fireballParticleEffect, ParticleEffect.class);
    }

    public void initLists() {
        // HEADS
        Texture[] head1 = new Texture[] {
                manager.get(Assets.head1_front, Texture.class),
                manager.get(Assets.head1_back, Texture.class)
        };
        Texture[] head2 = new Texture[] {
                manager.get(Assets.head2_front, Texture.class),
                manager.get(Assets.head2_back, Texture.class)
        };
        Assets.heads.put("head1", head1);
        Assets.heads.put("head2", head2);

        // ITEMS
        Assets.itemTextures.put(IronHelmet.class.getSimpleName(), new Texture[] {
                manager.get(Assets.ironHelmet_old_down, Texture.class),
                manager.get(Assets.ironHelmet_old_up, Texture.class),
        });
        Assets.itemTextures.put(IronBody.class.getSimpleName(), new Texture[]{
                manager.get(Assets.ironBody_front, Texture.class),
                manager.get(Assets.ironBody_back, Texture.class)
        });
        Assets.itemTextures.put(IronLegs.class.getSimpleName(), new Texture[]{
                manager.get(Assets.ironLegs_front, Texture.class),
                manager.get(Assets.ironLegs_back, Texture.class)
        });
        Assets.itemTextures.put(BeginnerBoots.class.getSimpleName(), new Texture[]{
                manager.get(Assets.beginnerBoots_front, Texture.class),
                manager.get(Assets.beginnerBoots_back, Texture.class)
        });
        Assets.itemTextures.put(BeginnerGloves.class.getSimpleName(), new Texture[]{
                manager.get(Assets.beginnerGloves_front, Texture.class),
                manager.get(Assets.beginnerGloves_back, Texture.class)
        });
        Assets.itemTextures.put(BeginnerCape.class.getSimpleName(), new Texture[]{
                manager.get(Assets.beginnerCape_down, Texture.class),
                manager.get(Assets.beginnerCape_up, Texture.class)
        });
        Assets.itemTextures.put(BeginnerStaff.class.getSimpleName(), new Texture[] {
                manager.get(Assets.beginnerStaff_down, Texture.class),
           manager.get(Assets.beginnerStaff_up, Texture.class)

        });
        Assets.itemTextures.put(BeginnerBow.class.getSimpleName(), new Texture[] {
                manager.get(Assets.beginnerBow_down, Texture.class),
                manager.get(Assets.beginnerBow_up, Texture.class)
        });
        initItem(new IronHelmet());
        initItem(new IronBody());
        initItem(new IronLegs());
        initItem(new BeginnerBoots());
        initItem(new BeginnerGloves());
        initItem(new BeginnerCape());
        initItem(new BeginnerStaff());
        initItem(new BeginnerBow());
    }

    public Texture getTexture(String name) {
        return manager.get(name, Texture.class);
    }

    public BitmapFont getFont(String name) {
        return manager.get(name, BitmapFont.class);
    }

    public Sound getSound(String name) {
        return manager.get(name, Sound.class);
    }

    public Skin getSkin(String name) {
        return manager.get(name, Skin.class);
    }

    public AnimatedSprite getAnimatedSprite(String name, boolean loop) {
        return new AnimatedSprite(manager.get(name, TextureAtlas.class), loop);
    }

    public ParticleEffect getParticleEffect(String pfPath) {
        return manager.get(pfPath);
    }

    public void dispose() { manager.dispose(); }

    void initItem(Item item){
        itemList.add(item);
    }

}
