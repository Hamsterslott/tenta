package com.nosheep.game.asset.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;

public class AnimatedSprite extends Sprite {

    private Animation animation;
    private float elapsedTime = 0;
    private TextureAtlas atlas;
    private boolean loop;
    private float frameDuration = 1/15f;

    private boolean isFastAnimation = false;

    public AnimatedSprite(TextureAtlas spriteSheet, boolean loop) {
        super();

        this.atlas = spriteSheet;
        this.loop = loop;

        animation = new Animation(frameDuration, atlas.getRegions());

        if(loop) animation.setPlayMode(Animation.PlayMode.LOOP);
        else animation.setPlayMode(Animation.PlayMode.NORMAL);
    }

    public void drawAnimatedSprite(SpriteBatch batch, int width, int height) {
            elapsedTime += Gdx.graphics.getDeltaTime();
            batch.draw(((TextureAtlas.AtlasRegion) animation.getKeyFrame(elapsedTime)), getX(), getY(), width, height);
    }

    public Animation getAnimation(){
        return animation;
    }

    public void setFastAnimation(boolean fastAnimation) {
        isFastAnimation = fastAnimation;
        animation.setFrameDuration(0.001f);
    }
}
