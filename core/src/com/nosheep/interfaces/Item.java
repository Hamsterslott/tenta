package com.nosheep.interfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Player;

/**
 * Created by Johan on 2017-08-16.
 */
public interface Item {
    void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position);
    int getId();
    Texture getTexture();
    String getName();
    String getType();
    int getPower();
    int getDefence();
    void setPower(int power);
    void setDefence(int defence);
}
