package com.nosheep.interfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.nosheep.handler.skill.SkillHandler;
import com.nosheep.handler.StatHandler;
import com.nosheep.model.Statistic;
import com.nosheep.model.skill.Skill;

public interface CombatUser {
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    String getName();
    String getLocation();
    float getCurrentHealth();
    float getMaximumHealth();
    int getLvl();
    CombatUser getTarget();
    boolean hasTarget();
    void setTarget(CombatUser combatUser);
    Texture getIcon();
    void setIcon(Texture icon);
    void dealDamage(boolean updateServer, int damage);
    boolean isAlive();
    String getId();
    void useSkill(Skill skill);
    StatHandler getStatHandler();
    SkillHandler getSkillHandler();
    Statistic getStats();
    int getPower();
    int getDefence();
    Rectangle getBounds();
}
