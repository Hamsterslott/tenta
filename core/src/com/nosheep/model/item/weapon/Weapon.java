package com.nosheep.model.item.weapon;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.interfaces.Item;
import com.nosheep.game.combatuser.Player;
import com.nosheep.model.Statistic;

/**
 * Created by Johan on 2017-08-16.
 */
public abstract class Weapon implements Item {

    public enum WeaponType {
        STAFF,
        BOW,
        SWORD
    }

    public Texture textures[];
    public Texture icon;

    public int id;
    public String type;
    public String name = "";

    public int width;
    public int height;

    private Statistic stats;
    private WeaponType weaponType;

    public Weapon(){
        stats = new Statistic();
        name = this.getClass().getSimpleName();

        init();
    }

    protected abstract void init();

    public void setStats(int power, int defence) {
        stats.power = power;
        stats.defence = defence;
    }

    @Override
    public void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position) {
        batch.draw(textures[direction.ordinal()], position.x, position.y, width, height);
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    @Override
    public Texture getTexture() {
        return textures[0];
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getPower() {
        return stats.power;
    }

    @Override
    public void setPower(int power) {
        stats.power = power;
    }

    @Override
    public int getDefence() {
        return stats.power;
    }

    @Override
    public void setDefence(int defence) {
        stats.defence = defence;
    }

}
