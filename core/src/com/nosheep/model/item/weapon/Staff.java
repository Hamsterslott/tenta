package com.nosheep.model.item.weapon;

public class Staff extends Weapon {
    @Override
    protected void init() {
        setWeaponType(WeaponType.STAFF);
        setWidth(38);
        setHeight(56);
    }
}
