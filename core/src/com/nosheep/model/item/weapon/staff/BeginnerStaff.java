package com.nosheep.model.item.weapon.staff;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.weapon.Staff;

public class BeginnerStaff extends Staff {

    public BeginnerStaff() {
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(10, 0);
    }

}
