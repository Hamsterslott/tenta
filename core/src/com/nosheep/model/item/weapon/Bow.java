package com.nosheep.model.item.weapon;

public class Bow extends Weapon {
    @Override
    protected void init() {
        setWeaponType(WeaponType.BOW);
        setWidth(38);
        setHeight(56);
    }
}
