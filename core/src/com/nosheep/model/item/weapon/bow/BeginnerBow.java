package com.nosheep.model.item.weapon.bow;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.weapon.Bow;

public class BeginnerBow extends Bow {

    public BeginnerBow() {
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(5, 0);
    }

}
