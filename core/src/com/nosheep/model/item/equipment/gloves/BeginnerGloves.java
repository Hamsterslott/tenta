package com.nosheep.model.item.equipment.gloves;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Gloves;

/**
 * Created by Johan on 2017-08-18.
 */
public class BeginnerGloves extends Gloves {

    public BeginnerGloves(){
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(1, 1);
    }

}
