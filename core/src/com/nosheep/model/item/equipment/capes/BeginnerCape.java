package com.nosheep.model.item.equipment.capes;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Cape;

/**
 * Created by Johan on 2017-08-18.
 */
public class BeginnerCape extends Cape {

    public BeginnerCape(){
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(1, 1);
    }
}
