package com.nosheep.model.item.equipment.body;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Body;

/**
 * Created by Johan on 2017-08-18.
 */
public class IronBody extends Body {

    public IronBody(){
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(2, 2);
    }

}
