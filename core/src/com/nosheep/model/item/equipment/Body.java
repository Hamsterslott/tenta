package com.nosheep.model.item.equipment;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Player;

/**
 * Created by Johan on 2017-08-16.
 */
public class Body extends Equipment {
    public Body(){
        super();
    }

    @Override
    protected void init() {
        type = "body";
        setWidth(40);
        setHeight(80);
    }

    @Override
    public void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position) {
        batch.draw(textures[direction.ordinal()], position.x, position.y, getWidth(), getHeight());
    }
}
