package com.nosheep.model.item.equipment.boots;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Boots;

/**
 * Created by Johan on 2017-08-18.
 */
public class BeginnerBoots extends Boots {

    public BeginnerBoots(){
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(1, 1);
    }

}
