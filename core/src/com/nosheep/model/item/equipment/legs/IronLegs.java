package com.nosheep.model.item.equipment.legs;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Legs;

/**
 * Created by Johan on 2017-08-18.
 */
public class IronLegs extends Legs {

    public IronLegs(){
        super();
        id = 2;
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(2, 2);
    }

}
