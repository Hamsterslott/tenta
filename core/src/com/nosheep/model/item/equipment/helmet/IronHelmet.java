package com.nosheep.model.item.equipment.helmet;

import com.nosheep.game.asset.Assets;
import com.nosheep.model.item.equipment.Helmet;

/**
 * Created by Johan on 2017-08-18.
 */
public class IronHelmet extends Helmet {

    public IronHelmet(){
        super();
        textures = Assets.getItemTextureByName(this.getClass().getSimpleName());
        setStats(2, 2);
    }

}
