package com.nosheep.model.item.equipment;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Player;

/**
 * Created by Johan on 2017-08-16.
 */
public class Cape extends Equipment {
    public Cape(){
        super();
    }

    @Override
    protected void init() {
        type = "cape";
        setWidth(35);
        setHeight(60);
    }

    @Override
    public void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position) {
        batch.draw(textures[direction.ordinal()], position.x + 3, position.y + 2, getWidth(), getHeight());
    }
}
