package com.nosheep.model.item.equipment;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.interfaces.Item;
import com.nosheep.game.combatuser.Player;
import com.nosheep.model.Statistic;

/**
 * Created by Johan on 2017-08-16.
 */
public abstract class Equipment implements Item {

    public Texture textures[];


    public int id;
    public String type;
    public String name;

    private int width;
    private int height;

    private Statistic stats;

    public Equipment(){
        stats = new Statistic();
        name = this.getClass().getSimpleName();

        init();
    }

    protected abstract void init();

    public void setStats(int power, int defence) {
        stats.power = power;
        stats.defence = defence;
    }

    @Override
    public abstract void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position);

    @Override
    public Texture getTexture() {
        return textures[0];
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getPower() {
        return stats.power;
    }

    @Override
    public void setPower(int power) {
        stats.power = power;
    }

    @Override
    public int getDefence() {
        return stats.power;
    }

    @Override
    public void setDefence(int defence) {
        stats.defence = defence;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
