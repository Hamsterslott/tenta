package com.nosheep.model.item.currency;

import com.nosheep.game.asset.Assets;
import com.nosheep.util.AssetUtil;

public class Coins extends Currency {

    private int originalAmount = 0;
    private int lootAmount = 0;

    public Coins(int amount) {
        this.originalAmount = amount;
        this.lootAmount = (int) ((Math.random() * originalAmount) + 1);
        setTexture(AssetUtil.getTexture(Assets.coins));
    }

    @Override
    public String getName() {
        return lootAmount + " " + this.getClass().getSimpleName();
    }

    public int getAmount() {
        return getLootAmount();
    }

    public void setLootAmount(int lootAmount) {
        this.lootAmount = lootAmount;
    }

    public int getLootAmount() {
       return lootAmount;
    }
}
