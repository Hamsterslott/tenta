package com.nosheep.model.item.currency;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Player;
import com.nosheep.interfaces.Item;

public class Currency implements Item {

    private Texture texture;

    @Override
    public void render(SpriteBatch batch, Player.DIRECTION direction, Vector2 position) {

    }

    @Override
    public int getId() {
        return -1;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public int getPower() {
        return 0;
    }

    @Override
    public int getDefence() {
        return 0;
    }

    @Override
    public void setPower(int power) {

    }

    @Override
    public void setDefence(int defence) {

    }
}
