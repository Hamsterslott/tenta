package com.nosheep.model;

public class Statistic {
    public float currentHealth;
    public float maxHealth;
    public int level;
    public int power;
    public int defence;
    public float currentExperience;
    public float maxExperience;
    public int movementSpeed;
    public int combatMovementSpeed;

    public Statistic() {
        currentHealth = 0;
        maxHealth = 1;
        level = 0;
        power = 0;
        defence = 0;
        currentHealth = 0;
        maxExperience = 1;
        movementSpeed = 0;
        combatMovementSpeed = 0;
    }
}
