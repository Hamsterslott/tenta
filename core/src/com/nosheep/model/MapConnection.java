package com.nosheep.model;

import com.badlogic.gdx.math.Vector2;

public class MapConnection {
    public String targetMap;
    public Vector2 transferPosition;
    public Vector2 targetMapPosition;

    public MapConnection(String targetMap, Vector2 transferPosition, Vector2 targetMapPosition) {
        this.targetMap = targetMap;
        this.transferPosition = transferPosition;
        this.targetMapPosition = targetMapPosition;
    }
}
