package com.nosheep.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.tool.Timer;

public class DamageRenderPackage {
    private Integer damage;
    private Vector2 startPosition;
    private Vector2 position;
    private Timer timer;
    private boolean isMoving;
    private boolean isActive;
    private BitmapFont font;
    private BitmapFont fontZeroHit;

    public DamageRenderPackage() {
        isMoving = true;
        isActive = true;
        font = AssetUtil.getFont(Assets.fntArialMedium);
        fontZeroHit = AssetUtil.getFont(Assets.fntArialMediumGray);
    }

    public void update() {
        if(isMoving) {
            Float distanceTraveled = position.y - startPosition.y;
            if(distanceTraveled > 50f)  {
                isMoving = false;
            }
            position.y += 8;
        }
        if(timer.getCurrentTime() > 1) {
            isActive = false;
        }
    }

    public void draw(SpriteBatch batch) {
        update();
        if(damage == 0)
            fontZeroHit.draw(batch, damage.toString(), position.x, position.y);
        else
            font.draw(batch, damage.toString(), position.x, position.y);
    }

    public Integer getDamage() {
        return damage;
    }

    public void setDamage(Integer damage) {
        this.damage = damage;
    }

    public Vector2 getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Vector2 startPosition) {
        this.startPosition = startPosition;
        this.position = new Vector2();
        this.position.x = startPosition.x;
        this.position.y = startPosition.y;
        this.timer = new Timer();
        timer.start();
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public boolean isActive() {
        return isActive;
    }
}
