package com.nosheep.model.skill;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.animation.AnimatedSprite;
import com.nosheep.interfaces.CombatUser;

import java.util.List;

public abstract class Skill {

    private boolean active = true;
    protected CombatUser skillUser;
    protected CombatUser skillTarget;
    private List<CombatUser> skillTargets;
    private int skillArea;
    private String name;
    private int damage;
    private boolean dealDamage = true;
    private int projectileSpeed;
    private int projectileDistance;
    private Vector2 travelVector;
    private int castingTime;
    private double cooldown;
    private float positionX, positionY;
    private int width, height;
    private int startingX, startingY;
    private int destinationX, destinationY;
    private AnimatedSprite animation;
    private AnimatedSprite hitAnimation;

    private boolean useParticleEffect = false;

    private ParticleEffect particleEffect;

    public Skill() {
        setName(this.getClass().getName());
    }

    public Skill(CombatUser sender) {
        this.skillUser = sender;
        initSkill();
    }

    public Skill(CombatUser sender, CombatUser target){
        this.skillUser = sender;
        this.skillTarget = target;
        initSkill();
    }

    public Skill(CombatUser sender, List<CombatUser> targets) {
        this.skillUser = sender;
        this.skillTargets = targets;
        initSkill();
    }

    private void initSkill(){
        setName(this.getClass().getName());
        setPositionX(skillUser.getX());
        setPositionY(skillUser.getY());
    }

    public abstract void init();

    protected void update(){
        if(particleEffect != null) {
            if(particleEffect.isComplete()) particleEffect.reset();
            particleEffect.setPosition(getPositionX(), getPositionY());
            particleEffect.getEmitters().first().setPosition(getPositionX(), getPositionY());
            particleEffect.update(Gdx.graphics.getDeltaTime());
        }
        if(animation != null) animation.setPosition(getPositionX(), getPositionY());
        if(hitAnimation != null) hitAnimation.setPosition(getPositionX(), getPositionY());
    }

    public void render(SpriteBatch batch, float delta){
        update();
        if(useParticleEffect) particleEffect.draw(batch, delta);
        else if(animation != null && active) animation.drawAnimatedSprite(batch, width, height);
    }
    public int getDestinationX() {
        return destinationX;
    }

    public void setDestinationX(int destinationX) {
        this.destinationX = destinationX;
    }

    public int getDestinationY() {
        return destinationY;
    }

    public void setDestinationY(int destinationY) {
        this.destinationY = destinationY;
    }

    public AnimatedSprite getAnimation() {
        return animation;
    }

    public void setAnimation(AnimatedSprite animation) {
        this.animation = animation;
    }

    public AnimatedSprite getHitAnimation() {
        return hitAnimation;
    }

    public void setHitAnimation(AnimatedSprite hitAnimation) {
        this.hitAnimation = hitAnimation;
    }

    public boolean isActive(){ return active; }
    public CombatUser getSkillUser(){
        return skillUser;
    }
    public void setSkillUser(CombatUser user){
        this.skillUser = user;
    }
    public CombatUser getSkillTarget(){
        return skillTarget;
    }
    public void setSkillTarget(CombatUser target){
        this.skillTarget = target;
    }
    public List<CombatUser> getSkillTargets(){
        return skillTargets;
    }
    public void setSkillTargets(List<CombatUser> targets){
        this.skillTargets = targets;
    }
    public int getSkillArea(){
        return skillArea;
    }
    public String getSkillName(){
        return name;
    }
    public int getSkillDamage(){
        return damage;
    }
    public int getProjectileSpeed(){
        return projectileSpeed;
    }
    public int getCastingTime(){
        return castingTime;
    }

    public int getProjectileDistance() {
        return projectileDistance;
    }

    public void setProjectileDistance(int projectileDistance) {
        this.projectileDistance = projectileDistance;
    }

    public int getStartingX() {
        return startingX;
    }

    public void setStartingX(int startingX) {
        this.startingX = startingX;
    }

    public int getStartingY() {
        return startingY;
    }

    public void setStartingY(int startingY) {
        this.startingY = startingY;
    }

    public Vector2 getTravelVector() {
        return travelVector;
    }

    public void setTravelVector(Vector2 travelVector) {
        this.travelVector = travelVector;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public int getWidth() {
        return width;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setSkillArea(int skillArea) {
        this.skillArea = skillArea;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setProjectileSpeed(int projectileSpeed) {
        this.projectileSpeed = projectileSpeed;
    }

    public void setCastingTime(int castingTime) {
        this.castingTime = castingTime;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isDealDamage() {
        return dealDamage;
    }

    public void setDealDamage(boolean dealDamage) {
        this.dealDamage = dealDamage;
    }

    public void setUseParticleEffect(boolean useParticleEffect) {
        this.useParticleEffect = useParticleEffect;
    }

    public ParticleEffect getParticleEffect() {
        return particleEffect;
    }

    public boolean isUseParticleEffect() {
        return useParticleEffect;
    }

    public void setParticleEffect(ParticleEffect particleEffect) {
        this.particleEffect = particleEffect;
        this.particleEffect.start();
    }

    public double getCooldown() {
        return cooldown;
    }

    public void setCooldown(double cooldown) {
        this.cooldown = cooldown;
    }

}
