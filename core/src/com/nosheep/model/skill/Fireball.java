package com.nosheep.model.skill;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.nosheep.game.asset.Assets;
import com.nosheep.handler.skill.ProjectileHandler;
import com.nosheep.util.AssetUtil;

public class Fireball extends ProjectileHandler {

    public Fireball() {
        super();
    }

    @Override
    protected void update() {
        super.update();
        updateProjectileSpeed();
    }

    private void updateProjectileSpeed() {
        double counterLimit = 0.5;
        if(timer.getCurrentTime() > counterLimit) setProjectileSpeed(getProjectileSpeed() + 1);
        else if (timer.getCurrentTime() > counterLimit * 2) setProjectileSpeed(getProjectileSpeed() + 2);
    }

    @Override
    public void init() {
        super.init();
        setProjectileSpeed(1);
        setProjectileDistance(600);
        setCastingTime(1);
        setCooldown(0.5);
        setDamage(10);
        setWidth(25);
        setHeight(25);
        setFollowTarget(false);
        setUseParticleEffect(true);
        ParticleEffect particleEffect = AssetUtil.getParticleEffect(Assets.fireballParticleEffect);
        setParticleEffect(particleEffect);
        setHitMultipleTargetsOnImpact(true);
    }
}
