package com.nosheep.model.skill;

import com.badlogic.gdx.Gdx;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.asset.animation.AnimatedSprite;
import com.nosheep.handler.skill.ProjectileHandler;
import com.nosheep.util.AssetUtil;

public class Arrow extends ProjectileHandler {

    public Arrow() {
        super();
    }

    @Override
    public void init() {
        super.init();
        setFollowTarget(false);
        setProjectileSpeed(30);
        setProjectileDistance(800);
        setCastingTime(1);
        setCooldown(0.1);
        setDamage(2);
        setWidth(15);
        setHeight(30);
        AnimatedSprite animatedSprite = AssetUtil.getAnimatedSprite(Assets.arrow_idle, true);
        animatedSprite.setFastAnimation(true);
        setAnimation(animatedSprite);
    }

    @Override
    protected void update() {
        super.update();
    }

}
