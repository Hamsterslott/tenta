package com.nosheep.model.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.interfaces.Item;
import com.nosheep.tool.Timer;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.InputUtil;

public class LootBox {

    private Sprite defaultIcon;
    private BitmapFont smallFont;
    private Vector2 lootPosition;
    private GlyphLayout layout = new GlyphLayout();
    private Timer timer;

    private Item lootItem;

    public LootBox() {
        smallFont = AssetUtil.getFont(Assets.fntNeuroPoliticalSmall);
        defaultIcon = new Sprite(AssetUtil.getTexture(Assets.defaultLootIcon));
        timer = new Timer();
        timer.start();
    }

    public void render(SpriteBatch batch) {
        batch.begin();
        if(defaultIcon != null) {
            defaultIcon.setPosition(lootPosition.x, lootPosition.y);
            defaultIcon.draw(batch);
            batch.draw(lootItem.getTexture(), lootPosition.x + 5, lootPosition.y + 5, 25, 25);

            if (InputUtil.isMouseHovering(defaultIcon.getBoundingRectangle())) {
                defaultIcon.setAlpha(1);
                layout.setText(smallFont, lootItem.getName());
                smallFont.draw(batch, layout, (lootPosition.x + defaultIcon.getWidth() / 2) - layout.width / 2, lootPosition.y + defaultIcon.getHeight() + 25);
            }
            else{
                defaultIcon.setAlpha(0.5f);
            }
        }
        batch.end();
    }

    public Sprite getDefaultIcon() {
        return defaultIcon;
    }

    public void setDefaultIcon(Sprite defaultIcon) {
        this.defaultIcon = defaultIcon;
    }

    public Vector2 getLootPosition() {
        return lootPosition;
    }

    public void setLootPosition(Vector2 position){
        this.lootPosition = position;
    }

    public Item getLootItem() {
        return lootItem;
    }

    public void setLootItem(Item lootItem) {
        this.lootItem = lootItem;
    }

    public boolean hasTimeExpired() {
        if(timer.getCurrentTime() >= 120)
            return true;
        else
            return false;
    }

    public Rectangle getBounds() {
        return new Rectangle(getLootPosition().x, getLootPosition().y, defaultIcon.getWidth(), defaultIcon.getHeight());
    }
}
