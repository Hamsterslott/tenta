package com.nosheep.model.inventory;

import com.nosheep.interfaces.Item;

import java.util.ArrayList;
import java.util.List;

public class Bag {

    int capacity;
    List<Item> items;

    public Bag(){
        items = new ArrayList<Item>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
