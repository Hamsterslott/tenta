package com.nosheep.model.inventory;

/**
 * Created by Johan on 2017-08-16.
 */
public class SmallBag extends Bag {

    public SmallBag(){
        super();
        capacity = 25;
    }

}
