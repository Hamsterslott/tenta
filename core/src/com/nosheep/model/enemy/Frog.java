package com.nosheep.model.enemy;

import com.nosheep.game.asset.Assets;
import com.nosheep.game.combatuser.Enemy;
import com.nosheep.interfaces.Item;
import com.nosheep.model.item.currency.Coins;
import com.nosheep.model.item.equipment.body.IronBody;
import com.nosheep.model.item.equipment.boots.BeginnerBoots;
import com.nosheep.model.item.equipment.capes.BeginnerCape;
import com.nosheep.model.item.equipment.gloves.BeginnerGloves;
import com.nosheep.model.item.equipment.helmet.IronHelmet;
import com.nosheep.model.item.equipment.legs.IronLegs;
import com.nosheep.model.item.weapon.bow.BeginnerBow;
import com.nosheep.model.item.weapon.staff.BeginnerStaff;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;

import java.util.ArrayList;
import java.util.List;

public class Frog extends Enemy {

    public Frog(){
        super();

        setIcon(AssetUtil.getTexture(Assets.icon_frog));
        setMaxHealth(100);
        setWidth(80);
        setHeight(100);
        setLvl(1);
        setPower(10);
        setDefence(15);
        setName("Frog");

        setIdleAnimation(
                AssetUtil.getAnimatedSprite(
                        Assets.frog_idle,
                        true)
        );
        setDeathAnimation(AssetUtil.getAnimatedSprite(
                Assets.death_anim_gravestone,
                false));
        List<Item> loot = new ArrayList<Item>();
        loot.add(GameUtil.getItemByName(IronHelmet.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(IronBody.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(IronLegs.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(BeginnerGloves.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(BeginnerBoots.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(BeginnerCape.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(BeginnerStaff.class.getSimpleName()));
        loot.add(GameUtil.getItemByName(BeginnerBow.class.getSimpleName()));
        setLoot(loot);

        setMaxCoinDrop(10);
    }

}
