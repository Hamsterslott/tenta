package com.nosheep.tool;

public class Strings {
    public static final String interactiveLoot = "Press 'F' to loot";
    public static final String bagIsFull = "Bag is full.";
    public static final String interactivePortal = "Press 'F' to enter";
}
