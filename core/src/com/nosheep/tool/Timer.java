package com.nosheep.tool;

public class Timer {

    private long startTime;
    private long elapsedTime;

    public void start() {
        elapsedTime = 0;
        reset();
    }

    public void reset() {
        startTime = System.currentTimeMillis();
    }

    public double getCurrentTime() {
        elapsedTime = System.currentTimeMillis() - startTime;
        double elapsedSeconds = elapsedTime / 1000.0;
        return elapsedSeconds;
    }

}
