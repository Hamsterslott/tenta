package com.nosheep.screen;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;

/**
 * Created by Johan Rasmussen on 2017-07-20.
 */
public class SplashScreen extends DefaultScreen {

    private Sprite splashSprite;

    private float opacity = 0.0f;

    private boolean splashDone = false;
    private boolean decreasing = false;

    public SplashScreen() {
        super();
        splashSprite = new Sprite(AssetUtil.getTexture(Assets.splashTexture));
        splashSprite.setPosition(GraphicUtil.WIDTH/2 - splashSprite.getWidth()/2,
                GraphicUtil.HEIGHT/2 - splashSprite.getHeight()/2);
        splashSprite.setScale(0.5f);
        splashSprite.setAlpha(opacity);
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        if(splashDone){
            GameUtil.GAME.setScreen(new LoginScreen());
        }

        if(!splashDone) {
            if(!decreasing) {
                increaseOpacity(delta);
            }
            else{
                decreaseOpacity(delta);
            }

            splashSprite.setAlpha(opacity);
        }
    }
    private void decreaseOpacity(float delta) {
        opacity -= 0.5 * delta;
        if(opacity <= 0){
            opacity = 0;
            splashDone = true;
        }
    }
    private void increaseOpacity(float delta) {
        opacity += 0.5 * delta;
        if (opacity >= 1) {
            opacity = 1;
            decreasing = true;
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();
        splashSprite.draw(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
