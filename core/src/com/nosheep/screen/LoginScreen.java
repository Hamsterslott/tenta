package com.nosheep.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.nosheep.game.asset.Assets;
import com.nosheep.screen.ui.component.NSTextButton;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;
import com.nosheep.screen.map.TutorialCave01;
import com.nosheep.handler.server.ServerHandler;
import com.nosheep.util.ServerUtil;

/**
 * Created by Johan on 2017-07-21.
 */
public class LoginScreen extends DefaultScreen {

    Sprite loginScreenBG;
    Sprite loginLogo;

    TextField txtUsername;
    NSTextButton connectNSTextButton;
    NSTextButton optionsNSTextButton;
    NSTextButton exitNSTextButton;

    public LoginScreen(){
        super();
        loginScreenBG = new Sprite(AssetUtil.getTexture(Assets.loginScreenBG));
        loginLogo = new Sprite(AssetUtil.getTexture(Assets.loginLogo));

        connectNSTextButton = new NSTextButton("Connect", new Vector2(10, GraphicUtil.HEIGHT / 2));
        optionsNSTextButton = new NSTextButton("Options", new Vector2(10, connectNSTextButton.getPosition().y - connectNSTextButton.getHeight() - 20));
        exitNSTextButton = new NSTextButton("Exit", new Vector2(10, optionsNSTextButton.getPosition().y - optionsNSTextButton.getHeight() - 20));

        txtUsername = new TextField("", skin);
        txtUsername.setPosition(10, connectNSTextButton.getPosition().y + connectNSTextButton.getHeight() + 20);
        txtUsername.setSize(300, 40);
        txtUsername.setText("newPlayer");

        addActor(txtUsername);
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        if(connectNSTextButton.isClicked()){
            ServerUtil.SERVER_HANDLER = new ServerHandler();
            ServerUtil.SERVER_HANDLER.Start();
            GameUtil.setMapScreen(new TutorialCave01());
        }
        if(optionsNSTextButton.isClicked()){

        }
        if(exitNSTextButton.isClicked()){
            Gdx.app.exit();
        }
        GameUtil.USERNAME = txtUsername.getText();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();
        loginScreenBG.draw(batch);
        loginLogo.draw(batch);
        connectNSTextButton.draw(batch);
        optionsNSTextButton.draw(batch);
        exitNSTextButton.draw(batch);
        batch.end();
        act(delta);
        draw();
    }
}
