package com.nosheep.screen.map;

import com.badlogic.gdx.math.Vector2;
import com.nosheep.util.GraphicUtil;

/**
 * Created by Johan on 2017-08-20.
 */
public class TutorialMap01 extends MapScreen {

    public TutorialMap01(){
        super();
        loadTMXMap("tmx/TutorialMap01.tmx");
        addMapConnection(TutorialCave01.class.getName(),
                new Vector2(GraphicUtil.WIDTH / 2 - mapConnectionSize + 120,
                30),
                new Vector2(GraphicUtil.WIDTH / 2 - mapConnectionSize + 30,
                        GraphicUtil.HEIGHT - game.player.getHeight() - 10));

        addMapConnection(BatCave.class.getName(),
                new Vector2(GraphicUtil.WIDTH - mapConnectionSize - 50,
                        GraphicUtil.HEIGHT / 2 - 100),
                new Vector2(game.player.getWidth() + 50,
                        GraphicUtil.HEIGHT / 2));
    }

}
