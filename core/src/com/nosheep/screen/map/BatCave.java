package com.nosheep.screen.map;

import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.GraphicUtil;

public class BatCave extends MapScreen {

    public BatCave() {
        super();
        loadMap(Assets.batCavetexture, new Vector2(500, 500));
        addMapConnection(TutorialMap01.class.getName(),
                new Vector2(mapConnectionSize + 20,
                        GraphicUtil.HEIGHT / 2),
                new Vector2(GraphicUtil.WIDTH - mapConnectionSize - 50,
                        GraphicUtil.HEIGHT / 2 - 100));
    }

}
