package com.nosheep.screen.map;

import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.GraphicUtil;

/**
 * Created by Johan on 2017-08-03.
 */
public class TutorialCave01 extends MapScreen {

    public TutorialCave01(){
        super();
       loadTMXMap("tmx/TutorialCave01.tmx");
        //loadMap(Assets.tutorialCave01texture,
          //      new Vector2(GraphicUtil.WIDTH / 2, 20));
        addMapConnection(TutorialMap01.class.getName(),
                new Vector2(GraphicUtil.WIDTH / 2 - mapConnectionSize + 30,
                GraphicUtil.HEIGHT - 60),
                new Vector2(GraphicUtil.WIDTH / 2 - mapConnectionSize + 120,
                        30));
    }

}
