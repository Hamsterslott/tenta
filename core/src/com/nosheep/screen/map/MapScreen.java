package com.nosheep.screen.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.combatuser.Enemy;
import com.nosheep.game.combatuser.Player;
import com.nosheep.game.asset.Assets;
import com.nosheep.model.MapConnection;
import com.nosheep.model.item.currency.Coins;
import com.nosheep.util.*;
import com.nosheep.model.DamageRenderPackage;
import com.nosheep.model.inventory.LootBox;
import com.nosheep.screen.DefaultScreen;
import com.nosheep.screen.ui.UI;
import com.nosheep.tool.Strings;

import java.util.*;

/**
 * Created by Johan on 2017-07-21.
 */
public class MapScreen extends DefaultScreen {

    public UI ui;

    float unitScale = 1f;
    protected Texture mapTexture;
    protected TiledMap tiledMap;
    private OrthogonalTiledMapRenderer mapRenderer;

    public List<MapObject> mapObjects = new ArrayList<MapObject>();
    public Vector2 startingPoint;

    public List<MapConnection> mapConnections;
    protected int mapConnectionSize = 50;

    public List<LootBox> loot = new ArrayList<LootBox>();
    private List<LootBox> lootToRemove = new ArrayList<LootBox>();
    public List<Enemy> enemies = new ArrayList<Enemy>();
    private List<Enemy> enemiesToRemove = new ArrayList<Enemy>();
    public List<DamageRenderPackage> damageToRender = new ArrayList<DamageRenderPackage>();
    public List<DamageRenderPackage> damageToRemoveFromRender = new ArrayList<DamageRenderPackage>();

    protected boolean instance = false;

    public MapScreen(){
        super();
        mapConnections = new ArrayList<MapConnection>();
        GameUtil.setCurrentMap(this);
        ui = new UI();
        enemies.clear();
        enemiesToRemove.clear();
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        ServerUtil.SERVER_HANDLER.update(delta);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        if(tiledMap != null){
            mapRenderer.render();
        }

        if(mapTexture != null) {
            batch.begin();
            batch.draw(mapTexture, 0, 0, getWidth(), getHeight());
            batch.end();
        }

        renderMapConnections();

        boolean pickedLootThisIteration = false;
        for(LootBox lb : loot){
            if(lb.hasTimeExpired()) {
                lootToRemove.add(lb);
                continue;
            }
            lb.render(batch);
            if(!pickedLootThisIteration) {
                pickedLootThisIteration = checkIfPlayerPickedLoot(lb);
            }
        }
        loot.removeAll(lootToRemove);

        try {
            for(Enemy e : enemies){
                if(!e.isAlive()) enemiesToRemove.add(e);
                e.render(batch, shapeRenderer, delta);
            }
        }
        catch(ConcurrentModificationException ex) {

        }
        enemies.removeAll(enemiesToRemove);
        enemiesToRemove.clear();

        if(!instance)
            renderPlayers(delta);

        if(game.player != null) {
            game.player.render(batch, delta);
            renderDamage();
            ui.render(batch, delta, game.player);
        }
    }

    private boolean checkIfPlayerPickedLoot(LootBox lb) {
        Vector2 tempV = new Vector2();
        tempV.x = lb.getLootPosition().x - lb.getDefaultIcon().getWidth();
        tempV.y = lb.getLootPosition().y - 10;
        if(GameUtil.isOverlaping(GameUtil.getPlayer().getBounds(),
                lb.getBounds())) {
            if(GameUtil.getPlayer().getBagHandler().isFull() && !(lb.getLootItem() instanceof Coins)) {
                GameUtil.currentMap.renderInteractWindow(batch, Strings.bagIsFull, tempV);
            } else {
                String text = Strings.interactiveLoot;
                GameUtil.currentMap.renderInteractWindow(batch, text, tempV);

                if(Gdx.input.isKeyJustPressed(Input.Keys.F) ||
                        (InputUtil.isMouseLeftClickedOutsideWindow() && InputUtil.isMouseHovering(lb.getBounds()))) {
                    if(lb.getLootItem() instanceof Coins) {
                        GameUtil.getPlayer().addCoinsToBag((Coins)(lb.getLootItem()));
                    } else
                        GameUtil.getPlayer().addItemToBag(lb.getLootItem());
                    GameUtil.currentMap.removeLootBox(lb);
                    return true;
                }
            }
        }
        return false;
    }

    public void renderInteractWindow(SpriteBatch batch, String text, Vector2 position) {
        batch.begin();
        batch.draw(AssetUtil.getTexture(Assets.targetbg), position.x - 10, position.y, text.length() + 150, -15);
        font.draw(batch, text, position.x, position.y);
        batch.end();
    }

    private void renderDamage() {
        batch.begin();
        for(DamageRenderPackage drp : damageToRender) {
            if(!drp.isActive())
                damageToRemoveFromRender.add(drp);
            else
                drp.draw(batch);
        }
        damageToRender.removeAll(damageToRemoveFromRender);
        batch.end();
    }

    private void renderMapConnections() {
        for(MapConnection m : mapConnections) {
           shapeRenderer.begin();
            if(isPlayerOverlapingMapConnection(new Rectangle(m.transferPosition.x, m.transferPosition.y, mapConnectionSize, mapConnectionSize))) {

                Vector2 tempV = new Vector2();
                tempV.x = m.transferPosition.x - 100;
                tempV.y = m.transferPosition.y;
                renderInteractWindow(batch, Strings.interactivePortal, m.transferPosition);

                if(Gdx.input.isKeyJustPressed(Input.Keys.F)) try {
                    changeMap((MapScreen) Class.forName(m.targetMap).newInstance(),
                            m.targetMapPosition);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            shapeRenderer.setColor(Color.WHITE);
            shapeRenderer.rect(
                    m.transferPosition.x,
                    m.transferPosition.y,
                    this.mapConnectionSize,
                    this.mapConnectionSize);
            shapeRenderer.end();
        }
    }

    private void renderPlayers(float delta){
        for(Iterator<Map.Entry<String, Player>> it = game.playerList.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Player> entry = it.next();
            Player p = entry.getValue();
            if(p.getLocation() == null) ServerUtil.SERVER_HANDLER.requestLocation(p);
            else {
                if(p.getLocation().equals(game.player.getLocation())) p.render(batch, delta);
                else it.remove();
            }
        }
    }

    private Boolean isPlayerOverlapingMapConnection(Rectangle mapConnection) {
        if(game.player == null) return false;
        else if(GameUtil.isOverlaping(game.player.getBounds(), mapConnection)) return true;
        return false;
    }

    public void removeLootBox(LootBox lootBox) {
        lootToRemove.add(lootBox);
    }

    protected void loadTMXMap(String path){
        tiledMap = new TmxMapLoader().load(path);
        mapRenderer = new OrthogonalTiledMapRenderer(tiledMap, unitScale);
        mapRenderer.setView(camera.combined, 0, 0, GraphicUtil.WIDTH, GraphicUtil.HEIGHT);
        for(MapObject object : tiledMap.getLayers().get("GameObjects").getObjects())
            mapObjects.add(object);

        MapObject startP = tiledMap.getLayers().get("Points").getObjects().get("startPoint");
        float startX = ((RectangleMapObject) startP).getRectangle().getX();
        float startY = ((RectangleMapObject) startP).getRectangle().getY();
        startingPoint = new Vector2(startX, startY);

        MapObject endP = tiledMap.getLayers().get("Points").getObjects().get("endPoint");
        float endX = ((RectangleMapObject) startP).getRectangle().getX();
        float endY = ((RectangleMapObject) startP).getRectangle().getY();
    }

    protected void loadMap(String path, Vector2 startingPoint) {
        this.mapTexture = AssetUtil.getTexture(path);
        this.startingPoint = startingPoint;
    }

    public void addMapConnection(String targetMap, Vector2 transferPosition, Vector2 targetMapPosition) {
        mapConnections.add(new MapConnection(targetMap, transferPosition, targetMapPosition));
    }

    protected void changeMap(MapScreen map, Vector2 targetMapPosition) {
        GameUtil.setMapScreen(map, targetMapPosition);
        ServerUtil.SERVER_HANDLER.setLocation();
    }

    @Override
    public boolean keyDown(int keycode) {
        return super.keyDown(keycode);

    }
}
