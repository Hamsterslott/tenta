package com.nosheep.screen.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nosheep.game.combatuser.Player;

/**
 * Created by Johan on 2017-08-03.
 */
public class ActionBar extends UIElement {

    public ActionBar(){
        super();
    }

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch batch, float delta, Player player) {
        super.render(batch, delta, player);
    }
}
