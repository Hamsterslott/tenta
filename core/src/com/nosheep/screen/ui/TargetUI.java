package com.nosheep.screen.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nosheep.game.asset.Assets;
import com.nosheep.interfaces.CombatUser;
import com.nosheep.game.combatuser.Player;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GraphicUtil;

public class TargetUI extends UIElement {

    private Texture healthBarGreen;
    private Texture healthBarYellow;
    private Texture healthBarRed;

    public TargetUI(){
        setTexture(AssetUtil.getTexture(Assets.targetbg));
        getBackground().flip(true, false);
        setWidth(300);
        setHeight(100);
        setPosition(GraphicUtil.WIDTH - getWidth(), GraphicUtil.HEIGHT - getHeight());
        initTextures();
    }

    private void initTextures() {
        healthBarGreen = AssetUtil.getTexture(Assets.healthBarGreen);
        healthBarYellow = AssetUtil.getTexture(Assets.healthBarYellow);
        healthBarRed = AssetUtil.getTexture(Assets.healthBarRed);
    }

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch batch, float delta, Player player) {
        super.render(batch, delta, player);
        batch.begin();
        batch.draw(background,
                getPositionX(),
                getPositionY(),
                getWidth(),
                getHeight());
        font.draw(batch, "NAME: " + player.getTarget().getName(), getPositionX() + 10, getPositionY() + getHeight() - 20);
        font.draw(batch, "LEVEL: " + player.getTarget().getLvl(), getPositionX() + 10, getPositionY() + getHeight() - 40);
        font.draw(batch, player.getTarget().getCurrentHealth() + " / ", getPositionX() + 10, getPositionY() + getHeight() - 60);
        font.draw(batch, player.getTarget().getMaximumHealth() + "", getPositionX() + 60, getPositionY() + getHeight() - 60);
        font.draw(batch, "LEVEL: " + player.getTarget().getLvl(), getPositionX() + 10, getPositionY() + getHeight() - 40);
        batch.draw(player.getTarget().getIcon(), getPositionX() + getWidth() - 80, getPositionY() + 10, 80, getHeight() - 10);
        renderUIHealthBar(batch, player.getTarget());
        batch.end();
    }
    private void renderUIHealthBar(SpriteBatch batch, CombatUser target){
        float healthPercentage = (target.getCurrentHealth() / target.getMaximumHealth()) * 100;
        if(healthPercentage < 0) healthPercentage = 0;
        if(healthPercentage > 70) batch.draw(healthBarGreen, getPositionX() + 10,getPositionY() + 10, healthPercentage * 2, 15);
        else if(healthPercentage > 30) batch.draw(healthBarYellow, getPositionX() + 10,getPositionY() + 10, healthPercentage * 2, 15);
        else batch.draw(healthBarRed, getPositionX() + 10,getPositionY() + 10, healthPercentage * 2, 15);
    }

}
