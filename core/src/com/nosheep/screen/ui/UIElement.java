package com.nosheep.screen.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.combatuser.Player;
import com.nosheep.util.AssetUtil;
import com.badlogic.gdx.graphics.Texture;
import com.nosheep.util.InputUtil;

/**
 * Created by Johan on 2017-08-03.
 */
public class UIElement {

    private boolean useHoverEffect;
    protected Sprite background;
    private float minimum_opacity = 0.8f;
    private float maximum_opacity = 1;
    private int positionX;
    private int positionY;
    private int width;
    private int height;
    protected BitmapFont font;

    public UIElement(){
        useHoverEffect = false;
        font = AssetUtil.getFont(Assets.fntNeuroPoliticalSmall);
    }

    public void update(float delta){
        if(useHoverEffect) {
            if (isHovering()) {
                background.setAlpha(maximum_opacity);
            }
            else{
                background.setAlpha(minimum_opacity);
            }
        }
    }

    public void render(SpriteBatch batch, float delta, Player player) {
        update(delta);
    }

    private boolean isHovering(){
        Vector2 mouse = InputUtil.getMouseCoordinates();
        if(background.getBoundingRectangle().contains(mouse))
            return true;
        return false;
    }

    public Sprite getBackground() {
        return background;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPosition(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setTexture(Texture texture) {
        background = new Sprite(texture);
    }

}
