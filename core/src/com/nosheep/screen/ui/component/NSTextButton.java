package com.nosheep.screen.ui.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.InputUtil;

/**
 * Created by Johan on 2017-07-23.
 */
public class NSTextButton {

    private Sprite sprite;
    private String text;
    private BitmapFont font;
    private Color fontColor;

    private int width = 200;
    private int height = 30;

    private boolean hasPlayedHoverSound = false;
    private Sound hover;
    private Sound click;

    public NSTextButton(String text, Vector2 position){
        sprite = new Sprite(AssetUtil.getTexture(Assets.loginScreenBG));
        sprite.setAlpha(0.5f);
        setPosition(position);
        font = AssetUtil.getFont(Assets.fntNeuroPolitical);
        GlyphLayout layout = new GlyphLayout();
        layout.setText(font, text);
        width = (int)layout.width;
        height = (int)layout.height;
        sprite.setSize(width, height);
        this.text = text;
        fontColor = font.getColor();
        setFontOpacity(0.5f);

        hover = AssetUtil.getSound(Assets.hover);
        click = AssetUtil.getSound(Assets.click);
    }

    public void draw(SpriteBatch batch){
        if(isHovering()) {
            if(!hasPlayedHoverSound) {
                hover.play(0.5f);
                hasPlayedHoverSound = true;
            }
            sprite.setAlpha(1);
            setFontOpacity(1);
        }
        else {
            hasPlayedHoverSound = false;
            sprite.setAlpha(0.5f);
            setFontOpacity(0.5f);
        }

        if(text != null){
            font.draw(batch, text, sprite.getX(), sprite.getY() + height);
        }

    }

    private boolean isHovering(){
        Vector2 mouse = InputUtil.getMouseCoordinates();
        if(sprite.getBoundingRectangle().contains(mouse))
            return true;
        return false;
    }

    public boolean isClicked(){
        if(isHovering() && Gdx.input.justTouched()) {
            click.play();
            return true;
        }
        return false;
    }

    private void setFontOpacity(float opacity){
        fontColor.set(fontColor.r, fontColor.g, fontColor.b, opacity);
    }

    public void setPosition(Vector2 position){
        sprite.setPosition(position.x, position.y);
    }

    public Vector2 getPosition(){
        return new Vector2(sprite.getX(), sprite.getY());
    }

    public int getHeight(){
        return height;
    }

    public int getWidth(){
        return width;
    }

}
