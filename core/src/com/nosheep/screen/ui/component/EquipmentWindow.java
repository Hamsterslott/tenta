package com.nosheep.screen.ui.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.combatuser.Player;
import com.nosheep.interfaces.Item;
import com.nosheep.model.inventory.LootBox;
import com.nosheep.model.item.weapon.Weapon;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.model.item.equipment.*;
import com.nosheep.util.InputUtil;

import java.util.ArrayList;
import java.util.List;

public class EquipmentWindow extends Window {

    private List<Item> itemsToRemove = new ArrayList<Item>();
    private Item[] equippedItems = new Item[7];

    public EquipmentWindow() {
        setToggleKey(Input.Keys.E);
    }

    @Override
    public void initWindow() {
        x = 200;
        y = 200;
        width = 200;
        height = 600;
    }

    @Override
    public void render(SpriteBatch batch, float delta) {
        super.render(batch, delta);
        if(!hidden) {
           updateItems();
            batch.begin();
            renderItems(batch, delta);
            batch.end();
        }
    }

    private void updateItems() {
        Player p = GameUtil.getPlayer();
        equippedItems[0] = p.getHelmet();
        equippedItems[1] = p.getBody();
        equippedItems[2] = p.getLegs();
        equippedItems[3] = p.getGloves();
        equippedItems[4] = p.getBoots();
        equippedItems[5] = p.getCape();
        equippedItems[6] = p.getWeapon();
    }

    private void renderItems(SpriteBatch batch, float delta) {
        Rectangle rect = new Rectangle();
        rect.x = x + 10;
        rect.y = y + height - 120;
        rect.width = 50;
        rect.height = 50;
        for(Item i : equippedItems) {
            if(InputUtil.isMouseHovering(rect)) {
                handleLeftClick(i);
            }
            batch.draw(AssetUtil.getTexture(Assets.healthBarRed), rect.x, rect.y, rect.width, rect.height);
            if(i != null)
                batch.draw(i.getTexture(), rect.x + 10, rect.y + 10, rect.width - 10, rect.height - 10);
            rect.y -= 65;
        }
        removeItems();
    }

    private void removeItems() {
        for(int i = 0; i < equippedItems.length; i++) {
            for(Item item : itemsToRemove) {
                if(item.equals(equippedItems[i])){
                    equippedItems[i] = null;
                    break;
                }
            }
        }
    }

    private void handleLeftClick(Item i) {
        if(Gdx.input.justTouched() &&
                Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            try {
                unEquipItem(i);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    private void unEquipItem(Item i) throws IllegalAccessException, InstantiationException {
        Player p = GameUtil.getPlayer();
        if(i == null || p.getBagHandler().isFull())
            return;
        p.addItemToBag(i.getClass().newInstance());
        itemsToRemove.add(i);
        if(i instanceof Weapon) {
            p.setWeapon(null);
        } else {
            if(i instanceof Helmet) {
                p.setHelmet(null);
            }
            else if(i instanceof Body) {
                p.setBody(null);
            }
            else if(i instanceof Legs) {
                p.setLegs(null);
            }
            else if(i instanceof Gloves) {
                p.setGloves(null);
            }
            else if(i instanceof Boots) {
                p.setBoots(null);
            }
            else if(i instanceof Cape) {
                p.setCape(null);
            }
        }
    }
}
