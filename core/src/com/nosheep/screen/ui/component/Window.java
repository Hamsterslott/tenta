package com.nosheep.screen.ui.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.nosheep.game.asset.Assets;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GraphicUtil;
import com.nosheep.util.InputUtil;

public abstract class Window {

    private Integer toggleKey;
    protected boolean hidden = true;
    protected int x, y, width, height;
    private Texture background;
    private Sprite closeButton;
    private Sprite topBar;

    private int mouseStartX;
    private int mouseStartY;
    private boolean isMovingWindow;
    protected BitmapFont font;

    public Window() {
        background = AssetUtil.getTexture(Assets.window_background);
        closeButton = new Sprite(AssetUtil.getTexture(Assets.window_close));
        topBar = new Sprite(AssetUtil.getTexture(Assets.window_top_bar));
        font = AssetUtil.getFont(Assets.fntNeuroPoliticalSmall);
    }

    public abstract void initWindow();

    private void update() {
        if(Gdx.input.isKeyJustPressed(toggleKey)) {
            if(hidden)
                hidden = false;
            else {
                hidden = true;
                isMovingWindow = false;
            }
        }
    }

    public void render(SpriteBatch batch, float delta) {
        update();
        if(!hidden) {
            batch.begin();
            renderBackground(batch);
            renderTopBar(batch);
            renderCloseButton(batch);
            batch.end();
        }
    }

    private void renderBackground(SpriteBatch batch) {
        batch.draw(background, x, y, width, height);
    }

    private void renderTopBar(SpriteBatch batch) {
        topBar.setBounds(x,
                y + height, width, -45);
        topBar.draw(batch);
        handleTopBarClicked(topBar.getBoundingRectangle());
    }

    private void handleTopBarClicked(Rectangle rect) {
        if(Gdx.input.justTouched() && InputUtil.isMouseHovering(rect)) {
            mouseStartX = (int) InputUtil.getMouseCoordinates().x;
            mouseStartY = (int) InputUtil.getMouseCoordinates().y;
            isMovingWindow = true;
        }

        if(Gdx.input.isTouched() &&
                isMovingWindow) {
            int mouseCurrentX = (int) InputUtil.getMouseCoordinates().x;
            int mouseCurrentY = (int) InputUtil.getMouseCoordinates().y;
            if(mouseCurrentX < mouseStartX) {
                x -= mouseStartX - mouseCurrentX;
                mouseStartX = mouseCurrentX;
            } else {
                x += mouseCurrentX - mouseStartX;
                mouseStartX = mouseCurrentX;
            }
            if(mouseCurrentY < mouseStartY) {
                y -= mouseStartY - mouseCurrentY;
                mouseStartY = mouseCurrentY;
            } else {
                y += mouseCurrentY - mouseStartY;
                mouseStartY = mouseCurrentY;
            }
        } else {
            isMovingWindow = false;
        }

        if(InputUtil.isMouseHovering(rect)) {
            topBar.setAlpha(1.0f);
        }
        else {
            topBar.setAlpha(0.2f);
        }
        if(x < 0)
            x = 0;
        else if(x + width > GraphicUtil.WIDTH)
            x = GraphicUtil.WIDTH - width;
        if(y < 0)
            y = 0;
        else if(y + height > GraphicUtil.HEIGHT)
            y = GraphicUtil.HEIGHT - height;
    }

    private void renderCloseButton(SpriteBatch batch) {
        closeButton.setBounds(x + width - 25 - 10,
                y + height - 25 - 10, 25, 25);
        closeButton.draw(batch);
        handleCloseButtonClicked(closeButton.getBoundingRectangle());
    }

    private void handleCloseButtonClicked(Rectangle rect) {
        if(InputUtil.isMouseHovering(rect)) {
            closeButton.setAlpha(1.0f);
            if(Gdx.input.justTouched()) {
                this.hidden = true;
            }
        }
        else {
            closeButton.setAlpha(0.7f);
        }
    }

    public void setToggleKey(Integer toggleKey) {
        this.toggleKey = toggleKey;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public boolean isHidden() {
        return hidden;
    }
}
