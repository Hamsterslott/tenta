package com.nosheep.screen.ui.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.combatuser.Player;
import com.nosheep.interfaces.Item;
import com.nosheep.model.inventory.LootBox;
import com.nosheep.model.item.weapon.Weapon;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.model.item.equipment.*;
import com.nosheep.util.InputUtil;

import java.util.ArrayList;
import java.util.List;

public class InventoryWindow extends Window {

    private List<Item> itemsToRemove = new ArrayList<Item>();
    private List<Item> items = new ArrayList<Item>();
    private boolean loadedItems = false;

    public InventoryWindow() {
        setToggleKey(Input.Keys.I);
    }

    @Override
    public void initWindow() {
        x = 800;
        y = 200;
        width = 600;
        height = 600;
    }

    @Override
    public void render(SpriteBatch batch, float delta) {
        super.render(batch, delta);
        if(!hidden) {
            if(!loadedItems) {
                items = GameUtil.getPlayer().getBagHandler().getItems();
                loadedItems = true;
            }
            batch.begin();
            renderItems(batch, delta);
            batch.end();
        }
    }

    private void renderItems(SpriteBatch batch, float delta) {
        Rectangle rect = new Rectangle();
        rect.x = x + 10;
        rect.y = y + height - 120;
        rect.width = 50;
        rect.height = 50;
        int index = 0;
        for(Item i : items) {
            if(i != null) {
                if(InputUtil.isMouseHovering(rect)) {
                    handleLeftClick(i);
                    handleRightClick(i);
                }
                batch.draw(AssetUtil.getTexture(Assets.healthBarYellow), rect.x, rect.y, rect.width, rect.height);
                batch.draw(i.getTexture(), rect.x + 10, rect.y + 10, rect.width - 10, rect.height - 10);
                rect.x += 65;
                if(index % 9 == 8){
                    rect.y -= 65;
                    rect.x = x + 10;
                }
            }
            index++;
        }
        drawCoins(batch);

        items.removeAll(itemsToRemove);
    }

    private void drawCoins(SpriteBatch batch) {
        batch.draw(AssetUtil.getTexture(Assets.coins), x + 20, y + 20, 25, 25);
        font.draw(batch, Integer.toString(GameUtil.getPlayer().getBagHandler().getCoins()), x + 50, y + 35);
    }

    private void handleLeftClick(Item i) {
        if(Gdx.input.justTouched() &&
                Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            try {
                equipItem(i);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleRightClick(Item i) {
        if(Gdx.input.justTouched() &&
                Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            dropItem(i);
        }
    }

    private void dropItem(Item i) {
        LootBox lb = new LootBox();
        try {
            lb.setLootItem(i.getClass().newInstance());
        } catch (InstantiationException e) {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return;
        }
        lb.setLootPosition(new Vector2(GameUtil.getPlayer().getX(), GameUtil.getPlayer().getY()));
        GameUtil.currentMap.loot.add(lb);
        itemsToRemove.add(i);
    }

    private void equipItem(Item i) throws IllegalAccessException, InstantiationException {
        Player p = GameUtil.getPlayer();
        if(i instanceof Weapon) {
            Weapon currentWeapon = p.getWeapon();
            p.setWeapon((Weapon)i.getClass().newInstance());
            if(currentWeapon == null)
                itemsToRemove.add(i);
            else
                items.set(items.indexOf(i), currentWeapon.getClass().newInstance());
        } else {
            if(i instanceof Helmet) {
                Helmet currentEquipment = p.getHelmet();
                p.setHelmet((Helmet)i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
            if(i instanceof Body) {
                Body currentEquipment = p.getBody();
                p.setBody((Body)i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
            if(i instanceof Legs) {
                Legs currentEquipment = p.getLegs();
                p.setLegs((Legs)i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
            if(i instanceof Gloves) {
                Gloves currentEquipment = p.getGloves();
                p.setGloves((Gloves) i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
            if(i instanceof Boots) {
                Boots currentEquipment = p.getBoots();
                p.setBoots((Boots) i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
            if(i instanceof Cape) {
                Cape currentEquipment = p.getCape();
                p.setCape((Cape) i.getClass().newInstance());
                if(currentEquipment == null)
                    itemsToRemove.add(i);
                else
                    items.set(items.indexOf(i), currentEquipment.getClass().newInstance());
                return;
            }
        }
    }
}
