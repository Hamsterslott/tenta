package com.nosheep.screen.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.combatuser.Player;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GraphicUtil;

/**
 * Created by Johan on 2017-08-03.
 */
public class PlayerInfo extends UIElement {

    private Texture experienceBarBackground;
    private Texture experienceBar;

    public PlayerInfo(){
        super();
        experienceBar = AssetUtil.getTexture(Assets.healthBarYellow);
        experienceBarBackground = AssetUtil.getTexture(Assets.experienceBarBackground);
    }

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    @Override
    public void render(SpriteBatch batch, float delta, Player player) {
        super.render(batch, delta, player);
        renderExperience(batch, player);
    }

    private void renderExperience(SpriteBatch batch, Player player) {
        batch.begin();
        float experiencePercentage = (player.getCurrentExperience() / player.getMaxExperience()) * 100;
        batch.draw(experienceBarBackground, 0, 0, GraphicUtil.WIDTH, 10);
        batch.draw(experienceBar, GraphicUtil.WIDTH / 2 - experiencePercentage * 9, 2, experiencePercentage * 18, 4);
        batch.end();
    }


}
