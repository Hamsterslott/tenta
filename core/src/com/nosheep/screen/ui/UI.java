package com.nosheep.screen.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.nosheep.game.combatuser.Player;
import com.nosheep.screen.ui.component.EquipmentWindow;
import com.nosheep.screen.ui.component.InventoryWindow;
import com.nosheep.screen.ui.component.Window;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Johan on 2017-08-03.
 */
public class UI {

    private PlayerInfo pi;
    private ActionBar ab;
    private TargetUI tui;
    public List<Window> windows;

    public UI(){
        ab = new ActionBar();
        pi = new PlayerInfo();
        tui = new TargetUI();
        windows = new ArrayList<Window>();

        windows.add(new InventoryWindow());
        windows.add(new EquipmentWindow());

        for(Window w : windows)
            w.initWindow();
    }

    public void render(SpriteBatch batch, float delta, Player player) {
        pi.render(batch, delta, player);
        ab.render(batch, delta, player);
        if(player.hasTarget()) tui.render(batch, delta, player);
        for(Window w : windows)
            w.render(batch, delta);
    }

}
