package com.nosheep.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.nosheep.game.asset.Assets;
import com.nosheep.game.TentaGame;
import com.nosheep.util.AssetUtil;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;

/**
 * Created by Johan on 2017-07-21.
 */
public abstract class DefaultScreen extends Stage implements Screen {

    protected OrthographicCamera camera;
    protected SpriteBatch batch;
    protected ShapeRenderer shapeRenderer;
    protected TentaGame game;
    protected Skin skin;
    protected BitmapFont font;

    public DefaultScreen(){
        game = GameUtil.GAME;
        Gdx.input.setInputProcessor(this);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, GraphicUtil.WIDTH, GraphicUtil.HEIGHT);
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setAutoShapeType(true);
        Viewport viewport = new StretchViewport(GraphicUtil.WIDTH, GraphicUtil.HEIGHT, camera);
        setViewport(viewport);
        skin = AssetUtil.getSkin(Assets.uiskin);
        font = AssetUtil.getFont(Assets.fntArialMediumGray);
    }

    public void update(float delta){

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
        getViewport().getCamera().update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode){
            case Input.Keys.F11:
                if(Gdx.graphics.isFullscreen())
                    GraphicUtil.setWindowedFullscreen();
                else
                    GraphicUtil.setFullScreen();
                break;
        }
        return false;
    }

}
