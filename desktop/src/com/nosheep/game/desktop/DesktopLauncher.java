package com.nosheep.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.nosheep.game.TentaGame;
import com.nosheep.util.GameUtil;
import com.nosheep.util.GraphicUtil;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = GameUtil.TITLE + GameUtil.VERSION;
		config.backgroundFPS = GraphicUtil.BACKGROUND_FPS;
		config.foregroundFPS = GraphicUtil.FOREGROUND_FPS;
		config.width = GraphicUtil.WIDTH;
		config.height = GraphicUtil.HEIGHT;
		config.addIcon("icon.png", Files.FileType.Internal);
		config.resizable = true;
		config.vSyncEnabled = true;
		config.useGL30 = true;
		config.useHDPI = true;
		config.allowSoftwareMode = true;

		new LwjglApplication(new TentaGame(), config);
	}
}
